#!/usr/bin/perl -w
# vim:sw=4:

# Copyright 2002-2006 RaphaÃ«l Hertzog <hertzog@debian.org>
# Copyright 2006-2007 Christoph Berg <myon@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use ConfirmationSpool;
use DB_File;

use strict;
use vars qw($spool_dir $conf_sub_template $conf_unsub_template
	    $conf_unsuball_template $sendmaildefault $sendmailnobody
	    %db_tags_content);

# Command line frontend for pts@qa.debian.org

require "common.pl";

my $email = ((getpwuid($<))[0]) . "\@debian.org";

# Lines of the mail we got
my @lines = @ARGV ? join " ", @ARGV : <>;

# Lines of the answer that we'll send
my @ans; # = ("Processing commands for pts\@qa.debian.org:\n", "\n");

foreach my $line (@lines) {
    #push @ans, "> $line\n";

    # Try to detect commands
    if ($line =~ /^\s*#/) {
	next;
	
    } elsif ($line =~ /^\s*subscribe\s+(\S+)(?:\s+(\S+))?/i) {
	my ($package, $address) = (lc($1), lc($2));
	$address = $email if (! (defined($address) && $address));
	my @explanation;
	($package, @explanation) = map_package($package);
	push @ans, @explanation;
	if (subscribe($address, $package)) {
	    push @ans, "$address has been subscribed to " .
		    "$package\@packages.qa.debian.org.\n";
	} else {
	    push @ans, "$address is already subscribed ...\n";
	}
	
    } elsif ($line =~ /^\s*unsubscribe\s+(\S+)(?:\s+(\S+))?/i) {
	my ($package, $address) = (lc($1), lc($2));
	$address = $email if (! (defined($address) && $address));
	if (unsubscribe($address, $package)) {
	    # users might still be subscribed to a renamed package
	    push @ans, "$address has been unsubscribed from " .
		    "$package\@packages.qa.debian.org.\n";
	} else {
	    # try the canonical name
	    my ($new_package, @explanation) = map_package($package);
	    push @ans, @explanation;
	    if (unsubscribe($address, $new_package)) {
		push @ans, "$address has been unsubscribed from " .
			"$new_package\@packages.qa.debian.org.\n";
	    } else {
		push @ans, "$address is not subscribed, you can't unsubscribe.\n";
	    }
	}
	
    } elsif ($line =~ /^\s*unsubscribeall(?:\s+(\S+))?/i) {
	my $address = lc($1);
	$address = $email if (! (defined($address) && $address));
	my @explanation;
	push @ans, "All your subscriptions have been terminated :\n";
	foreach my $package (which($address)) {
	    if (unsubscribe($address, $package)) {
		push @ans, "$address has been unsubscribed from " .
		    "$package\@packages.qa.debian.org.\n";
	    } else {
		push @ans, "$address is not subscribed, you can't unsubscribe.\n";
	    }
	}
	
    } elsif ($line =~ /^\s*confirm\s+(\S+)/i) {
#	my $key = $1;
#	next if (defined($done{"CONFIRM $key"})); # Not twice..
#	if (defined($cmd)) {
#	    my ($package, $address);
#	    if ($cmd =~ /^SUBSCRIBE (\S+) (\S+)/) {
#		($package, $address) = (lc($1), lc($2));
#		if (subscribe($address, $package)) {
#		    push @ans, "$address has been subscribed to " .
#			    "$package\@packages.qa.debian.org.\n";
#		    $subject = "You are now subscribed to $package";
#		} else {
#		    push @ans, "$address is already subscribed ...\n";
#		}
#	    } elsif ($cmd =~ /^UNSUBSCRIBE (\S+) (\S+)/) {
#		($package, $address) = (lc($1), lc($2));
#		if (unsubscribe($address, $package)) {
#		    push @ans, "$address has been unsubscribed from " .
#			    "$package\@packages.qa.debian.org.\n";
#		    $subject = "You are no longer subscribed to $package";
#		} else {
#		    push @ans, "$address is not subscribed, you can't unsubscribe.\n";
#		}
#	    } elsif ($cmd =~ /^UNSUBSCRIBEALL (\S+)/) {
#		$address = lc($1);
#		push @ans, "All your subscriptions have been terminated :\n";
#		foreach my $package (which($address)) {
#		    if (unsubscribe($address, $package)) {
#			push @ans, "$address has been unsubscribed from " .
#			    "$package\@packages.qa.debian.org.\n";
#			$subject = "All your subcriptions have been terminated";
#		    } else {
#			push @ans, "$address is not subscribed, you can't unsubscribe.\n";
#		    }
#		}
#	    } else {
#		push @ans, "Confirmation failed. Retry with a new command.\n";
#	    }
#	    $done{"CONFIRM $key"} = 1;
#	    #push @cc, $address if ($address ne $email);
#	} else {
	    push @ans, "Confirmation failed. Retry with a new command.\n";
#	}
	push @ans, "\n";
	
    } elsif ($line =~ /^\s*which(?:\s+(\S+))?/i) {

	my $address = lc($1);
	$address = $email if (! (defined($address) && $address));
	my $default_tags = join ',', get_default_tags($address);
	push @ans, "Here's the list of subscriptions for $address :\n";
	push @ans, map {
	    my $tags = join ',', get_tags($address, $_);
	    "* $_" . ($tags ne $default_tags ? " [$tags]" : "") . "\n"
	} (which($address));
	push @ans, "\n";
	
    } elsif ($line =~ /^\s*(?:list|who)\s+(\S+)/i) {

	my $package = lc($1);
	push @ans, "Here's the list of subscribers to $package :\n";
	push @ans, map { $_ . "\n" } (list($package));
	push @ans, "\n";

    } elsif ($line =~ /^\s*(?:keyword|tag)s?(?:\s+(\S+@\S+))?\s*$/i) {

	my $address = lc($1);
	$address = $email if (! (defined($address) && $address));
	push @ans, "Here's the default list of accepted keywords " .
		   "for $address :\n";
	push @ans, map { "* " . $_ . "\n" } (get_default_tags($address));
	push @ans, "\n";

    } elsif ($line =~ /^\s*(?:keyword|tag)s?\s+(\S+)(?:\s+(\S+@\S+))?\s*$/i) {

	my $package = lc($1);
	my $address = lc($2);
	$address = $email if (! (defined($address) && $address));
	push @ans, "Here's the list of accepted keywords associated to " . 
	           "package\n";
	push @ans, "$package for $address :\n";
	push @ans, map { "* " . $_ . "\n" } (get_tags($address, $package));
	push @ans, "\n";

    } elsif ($line =~ /^\s*(?:keyword|tag)s?(?:\s+(\S+@\S+))?\s+([-+=])\s+(\S+(?:\s+\S+)*)\s*$/i) {

	my $address = lc($1);
	$address = $email if (! (defined($address) && $address));
	my $cmd = $2;
	my @t = split(/[,\s]+/, lc($3));
	foreach (@t) {
	    push @ans, "WARNING: $_ is not a valid keyword.\n"
						    if (! is_valid_tag($_));
	}
	open_db_write();
	if ($cmd eq "=") {
	    set_default_tags($address, @t);
	} elsif ($cmd eq "+") {
	    my @tags = get_default_tags($address);
	    push @tags, @t;
	    set_default_tags($address, @tags);
	} elsif ($cmd eq "-") {
	    my $check = sub {
				foreach my $t (@t) {
				    return 0 if ($_[0] eq $t);
				}
				return 1;
			    };
	    my @tags = grep { &$check($_) } (get_default_tags($address));
	    set_default_tags($address, @tags);
	}
	push @ans, "Here's the new default list of accepted keywords " .
		   "for $address :\n";
	push @ans, map { "* " . $_ . "\n" } (get_default_tags($address));
	push @ans, "\n";
	close_db();

    } elsif ($line =~ /^\s*(?:keyword|tag)s?all(?:\s+(\S+@\S+))?\s+([-+=])\s+(\S+(?:\s+\S+)*)\s*$/i) {

	my $address = lc($1);
	$address = $email if (! (defined($address) && $address));
	my $cmd = $2;
	my @t = split(/[,\s]+/, lc($3));
	foreach (@t) {
	    push @ans, "WARNING: $_ is not a valid keyword.\n"
						    if (! is_valid_tag($_));
	}
	open_db_write();
	if (not exists $db_tags_content{$address}) {
	    my @tags = get_default_tags($address);
	    set_default_tags($address, @tags);
	}
	foreach (sort keys %db_tags_content) {
	    if (/^\Q$address\E(?:#([^#]+))?$/) {
		my $package = (defined($1) && $1) ? $1 : "";
		if ($cmd eq "=") {
		    if ($package) {
			set_tags($address, $package, @t);
		    } else {
			set_default_tags($address, @t);
		    }
		} elsif ($cmd eq "+") {
		    if ($package) {
			my @tags = get_tags($address, $package);
			push @tags, @t;
			set_tags($address, $package, @tags);
		    } else {
			my @tags = get_default_tags($address);
			push @tags, @t;
			set_default_tags($address, @tags);
		    }
		} elsif ($cmd eq "-") {
		    my $check = sub {
					foreach my $t (@t) {
					    return 0 if ($_[0] eq $t);
					}
					return 1;
				    };
		    if ($package) {
			my @tags = grep { &$check($_) } (get_tags($address, $package));
			set_tags($address, $package, @tags);
		    } else {
			my @tags = grep { &$check($_) } (get_default_tags($address));
			set_default_tags($address, @tags);
		    }
		}
		if ($package) {
		    push @ans, "Updated the list of keywords accepted by $address ". 
			       "for the package $package.\n";
		} else {
		    push @ans, "Updated the default list of keywords accepted by $address.\n";
		}
	    }
	}
	close_db();

    } elsif ($line =~ /^\s*(?:keyword|tag)s?\s+(\S+)(?:\s+(\S+@\S+))?\s+([-+=])\s+(\S+(?:\s+\S+)*)\s*$/i) {

	my $package = lc($1);
	my $address = lc($2);
	$address = $email if (! (defined($address) && $address));
	my $cmd = $3;
	my @t = split(/[,\s]+/, lc($4));
	foreach (@t) {
	    push @ans, "$_ is not a valid keyword.\n" if (! is_valid_tag($_));
	}
	open_db_write();
	if ($cmd eq "=") {
	    set_tags($address, $package, @t);
	} elsif ($cmd eq "+") {
	    my @tags = get_tags($address, $package);
	    push @tags, @t;
	    set_tags($address, $package, @tags);
	} elsif ($cmd eq "-") {
	    my $check = sub {
				foreach my $t (@t) {
				    return 0 if ($_[0] eq $t);
				}
				return 1;
			    };
	    my @tags = grep { &$check($_) } (get_tags($address, $package));
	    set_tags($address, $package, @tags);
	}
	push @ans, "Here's the new list of accepted keywords associated to " . 
	           "package\n";
	push @ans, "$package for $address :\n";
	push @ans, map { "* " . $_ . "\n" } (get_tags($address, $package));
	push @ans, "\n";
	close_db();
	
    } elsif ($line =~ /^\s*help/i) {
	push @ans, <DATA>;
    
    } elsif ($line =~ /^(--|\s*quit|\s*thanks?|\s*txs)/i) {
	push @ans, "Stopping processing here.\n";
	last;
	
    } else {
	push @ans, "Invalid command.\n";
    }
}

print join "", @ans;


__DATA__

Debian Package Tracking System
------------------------------

The Package Tracking System (PTS) has the following commands:

subscribe <srcpackage> [<email>]
  Subscribes <email> to all messages regarding <srcpackage>. If
  <email> is not given, it subscribes the From address. If the
  <srcpackage> is not a valid source package, you'll get a warning.
  If it's a valid binary package, the mapping will automatically be
  done for you.

unsubscribe <srcpackage> [<email>]
  Unsubscribes <email> from <srcpackage>. Like the subscribe command,
  it will use the From address if <email> is not given.

unsubscribeall [<email>]
  Cancel all subscriptions of <email>. Like the subscribe command,
  it will use the From address if <email> is not given.

which [<email>]
  Tells you which packages <email> is subscribed to.

keyword [<email>]
  Tells you the keywords that you are accepting. Each mail sent through
  the Package Tracking System is associated to a keyword and you receive
  only the mails associated to keywords that you are accepting. Here is
  the list of available keywords :
  * bts: mails coming from the Debian Bug Tracking System
  * bts-control: mails sent to control@bugs.debian.org
  * summary: automatic summary mails about the state of a package
  * cvs: notification of cvs commits
  * ddtp: notification of translations from the DDTP (cf ddtp.debian.org)
  * derivatives: notification of changes in derivative distributions
  * upload-source: announce of a new source upload that has been installed
  * upload-binary: announce of a new binary-only upload (porting)
  * katie-other: other mails from ftpmasters (override disparity, etc.)
  * default: all the other mails (those which aren't "automatic")
  By default you have the following keywords : bts, bts-control, summary,
  upload-source, katie-other, default.
  
keyword <srcpackage> [<email>]
  Same as previous item but for the given source package since
  you may select a different set of keywords for each source package.

keyword [<email>] {+|-|=} <list of keywords>
  Accept (+) or refuse (-) mails associated to the given keyword(s).
  Define the list (=) of accepted keywords.
  
keyword <srcpackage> [<email>] {+|-|=} <list of keywords>
  Same as previous item but overrides the keywords list for the indicated
  source package.
  
quit
thanks
  Stops processing commands.

