#!/usr/bin/perl -w

# Copyright 2008 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use DB_File;

use strict;
use vars qw(%db_content $db);

require "common.pl";

# Get the list of subscribers
open_db_read();
my $wantmail = sub {
    my ($address, $package) = @_;
    foreach (get_tags($address, $package)) {
	return 1 if ($_ eq "summary");
    }
    return 0;
};

my %done;
foreach my $pkg (sort keys %db_content) {
	next if $done{$pkg};
	my @emails = $db->get_dup($pkg);
	@emails = grep { &$wantmail($_, $pkg) } @emails;
	next unless scalar(@emails);
	print "$pkg\t" . join(", ", @emails) . "\n";
	$done{$pkg} = 1;
}
close_db();

