#!/usr/bin/perl -w

# Copyright 2002 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use DB_File;

use strict;
use vars qw(%db_content $db);

require "common.pl";

# Extract the list of packages and emails and print it
open_db_read();
my %hash;
foreach (keys %db_content) {
    $hash{$_} = 1;
}
foreach (keys %hash) {
    my @emails = $db->get_dup($_);
    print "$_ => [ @emails ]\n";
}
close_db();
