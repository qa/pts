#!/usr/bin/perl -w

# Copyright 2006 Raphael Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use lib '/srv/packages.qa.debian.org/perl';

use DB_File;
use strict;

require "common.pl";

my $email = shift;
my $count = 0;

foreach my $pkg (which($email)) {
    if (unsubscribe($email, $pkg)) {
	print "Unsubscription of $email from $pkg.\n";
	$count++;
    }
}
if (! $count) {
    print "No subscriptions cancelled (bad email?).\n";
    print "Usage: bin/unsub-all.pl <email>\n";
}

