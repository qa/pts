#!/bin/bash

# Copyright 2012 Olivier Berger & Institut Mines Telecom
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

# Generates an adms.sw 1.0 compatible Software Repository descriptor for the PTS

PATH="/bin:/sbin:/usr/bin:/usr/sbin"

if [ -d "incoming" ]; then
    root="$PWD"
elif [ -d "../incoming" ]; then
    root="$PWD/.."
else
    root="/srv/packages.qa.debian.org/www"
fi

cd $root/base
date=`LC_ALL=C date -u "+%FT%T%:z"`


# Generate a Turtle version

cat <<EOF >$root/web/common/admssw-repository.ttl
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix doap: <http://usefulinc.com/ns/doap#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix schema: <http://schema.org/> .
@prefix spdx: <http://www.spdx.org/rdf/terms#> .
@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix admssw: <http://purl.org/adms/sw/> .

<>
    a foaf:Document ;
    foaf:maker <#pts> ;
    dcterms:modified "$date" ;
    foaf:primaryTopic <#admsswrepo> .

# The Software Package Repository as meant by ADMS.SW
<#admsswrepo>
    a admssw:SoftwareRepository ;
    rdfs:label "Debian PTS" ;
    dcterms:description "Debian source packages descriptions from the Debian Package Tracking System (PTS)" ;
    dcterms:modified "$date" ;
    dcterms:publisher <http://www.debian.org/#project> ;
    adms:accessURL <http://packages.qa.debian.org/> ;
    adms:supportedSchema "ADMS.SW v1.0" .

# The PTS itself
<#pts>
    a foaf:Agent ;
    rdfs:label "Debian Package Tracking System" ;
    foaf:name "Debian PTS" .

<http://www.debian.org/#project>
    a admssw:SoftwareProject ;
    doap:name "Debian" ;
    doap:shortdesc "Debian" ;
    doap:description "The Debian project" ;
    doap:homepage <http://www.debian.org/> .

EOF

