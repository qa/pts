#!/bin/sh -e

# Copyright 2002 Raphaël Hertzog
# Copyright 2007-2008 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

umask 002

if [ -d "../incoming" ]; then
    root=$PWD/..
elif [ -d "incoming" ]; then
    root=$PWD
else
    root=/srv/packages.qa.debian.org/www
fi

cd $root

# Collect all the data from everywhere
bin/update_incoming.sh

# Convert everything to XML files
bin/sources_to_xml.py
# FIXME: re-enable when this is re-written to use the YAML formatted excuses
#bin/excuses_to_xml.py
bin/list_packages.sh | bin/other_to_xml.py

# Generate HTML and RDF pages with XSLT
bin/list_packages.sh | bin/generate_html.sh


# Generate a full RDF dump of the RDF pages

# As the full dump is really big and may be hard to consume, I prefer
# to generate an archive containing smaller RDF documents.

rm -f web/full-dump.*
rm -fr web/ttl-full-dump
mkdir web/ttl-full-dump

# There are 2 options : either each individual files
# (prepare_ttl_archivedir.sh) or hash-based dumps (which I prefer)
#bin/list_packages.sh | prepare_ttl_archivedir.sh
bin/list_packages.sh | bin/concatenate_ttl.sh
(cd web ; tar jcf full-dump.tar.bz2 ttl-full-dump)
rm -fr web/ttl-full-dump

# If that is OK for most, maybe make it available publicly
#mv $allrdffile.bz2 $root/web/common/full-dump.ttl.bz2

