#!/bin/bash

# Copyright © 2012, 2013 Olivier Berger & Institut Mines Telecom

# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

# This script will prepare the contents of the full dump archive

if [ -d "incoming" ]; then
    root="$PWD"
elif [ -d "../incoming" ]; then
    root="$PWD/.."
else
    root="/srv/packages.qa.debian.org/www"
fi

if [ "$1" = "-v" -o "$2" = "-v" ]; then
    verbose=1
fi

cd $root/base

allrdfdir=$root/web/ttl-full-dump

if [ ! -d $root/web/ttl-full-dump ]; then
    echo "Error: directory $root/web/ttl-full-dump not found" >&2
    exit 1
fi

# Read source package names on stdin
while read package
do
    hash=${package:0:1}
    if [ "${package:0:3}" = "lib" ]; then
	hash=${package:0:4}
    fi

    outputfile=$allrdfdir/dump-$hash.ttl
    dir=$hash/$package
    rdffile=$root/web/$dir.ttl
  
    if [ -n "$verbose" ]; then
	    echo "Adding $rdffile to $allrdfdir/$outputfile ..."
    fi
    if [ ! -f $outputfile ]; then
    	# the file is initiated with the @prefix header and contents of the first file
    	echo > $outputfile
    	echo "# $rdffile" >> $outputfile
    	cat $rdffile >> $outputfile
    	echo >> $outputfile
    else
    	# for second file and later, remove all @prefix header keeping only resources
    	echo >> $outputfile
    	echo "# $rdffile" >> $outputfile
    	grep -v '^@prefix' $rdffile >> $outputfile
    	echo >> $outputfile
    fi
done

rdffile=$root/web/common/admssw-repository.ttl
if [ -n "$verbose" ]; then
    echo "Copying $rdffile to $allrdfdir/dump-common.ttl ..."
fi
cp $rdffile $allrdfdir/dump-common.ttl

