#!/usr/bin/python
# -*- coding: utf-8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

# Copyright: © 2002-2011 Raphaël Hertzog
# Copyright: © 2005 Jeroen van Wolffelaar
# Copyright: © 2007-2009 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import os.path, sys, string, re, email, common, cPickle, yaml
try: import cjson as json
except ImportError: import json
from lxml import html
import xml.dom
import re
from datetime import datetime

try:
    from debian import deb822, debian_support
except: # Required to run on lenny
    from debian_bundle import deb822, debian_support

from config import dir, odir, root
from common import hash_name

def read_testing_info(fname):
    f = open(fname)
    data = json.load(f)
    f.close()
    return dict([(source['package'], source['status']) for source in data])

def read_font_reviews(fname):
    y = yaml.load(file(fname), yaml.CSafeLoader)
    packages = {} # maps pkg to the font review for it
    if y:
        for pkg, data in y.iteritems():
            if data.has_key('url'):
                packages[pkg] = data['url']
    return packages

def read_duck(fname):
    with open(fname) as f:
        return [line.strip() for line in f]

def read_upstream_info(fname):
    sources = set()
    with open(fname) as f:
        for line in f:
            line = line.strip()
            if line.endswith('.upstream'):
                source = line.split('/')[1].split('.')[0]
                sources.add(source)
    return list(sources)


def read_screenshots(fname):
    f = open(fname)
    data = json.load(f)
    f.close()
    sources = {}
    packages = data['packages']
    for package in packages:
        name = package['name']
        if name not in binary2sources: continue
        source = binary2sources[name]['current']
        if source is None: continue
        if source not in sources: sources[source] = []
        sources[source].append(name)
    return sources

def read_dedup(fname):
    sources = {}
    with open(fname) as f:
        for line in f:
            if line.startswith('#'): continue
            source, pkg_a, size, pkg_b = line.split()
            if source not in sources: sources[source] = []
            sources[source].append((size, pkg_a, pkg_b))
    return sources

def read_shortdesc(fname):
    global binary2sources
    source_descs = {} # source package -> (binary package -> short description)
    for line in open(fname):
        pkg, shortdesc = line.strip().split("\t", 1)
        if not binary2sources.has_key(pkg):
            continue
        src = binary2sources[pkg]['current']
        if src is None: srces = []
        else: srces = [src]
        for suite in ('unstable', 'experimental', 'testing', 'stable', 'oldstable', 'merged'):
             if suite in binary2sources[pkg]:
                 srces += binary2sources[pkg][suite]
        srces = list(set(srces))
        if not srces: continue
        for src in srces:
            if not source_descs.has_key(src):
                source_descs[src] = {}
            source_descs[src][pkg] = shortdesc
    return source_descs

def read_longdesc(fname):
    global binary2sources
    f = open(fname)
    data = json.load(f)
    f.close()
    source_descs = {} # source package -> (binary package -> long description)
    for pkg, longdesc in data.items():
        if not binary2sources.has_key(pkg):
            continue
        src = binary2sources[pkg]['current']
        if src is None: srces = []
        else: srces = [src]
        for suite in ('unstable', 'experimental', 'testing', 'stable', 'oldstable', 'merged'):
             if suite in binary2sources[pkg]:
                 srces += binary2sources[pkg][suite]
        srces = list(set(srces))
        if not srces: continue
        for src in srces:
            if not source_descs.has_key(src):
                source_descs[src] = {}
            source_descs[src][pkg] = longdesc
    return source_descs

def read_bug_summary(fname):
    global binary2sources
    summary = {} # source package -> bug count
    for line in open(fname):
        pkg, count = line.split()
        if not binary2sources.has_key(pkg):
            continue
        src = binary2sources[pkg]['current']
        if src is None:
            continue
        if not summary.has_key(src):
            summary[src] = 0
        summary[src] += int(count)
    return summary

def read_low_threshold_nmu(fname):
    """read low threshold NMU info"""
    emails = []
    if os.path.exists(fname):
        f = open(fname)
        devel_php_RE = \
            re.compile(r'https?://qa\.debian\.org/developer\.php\?login=([^\s&|]+)')
        word_RE = re.compile(r'^\w+$')
        for line in f.readlines():
            match = devel_php_RE.search(line)
            while match:    # look for several matches on the same line
                email = None
                login = match.group(1)
                if word_RE.match(login):
                    email = login + '@debian.org'
                elif login.find('@') >= 0:
                    email = login
                if email:
                    emails.append(email)
                line = line[match.end():]
                match = devel_php_RE.search(line)
        f.close()
    return emails

def read_transitions(rejectlist, pkglist):
    yaml_reject = yaml.safe_load(file(rejectlist))
    yaml_pkglist = yaml.safe_load(file(pkglist))
    packages = {} # maps pkg to the transitions they are involved in
    for id, transition in yaml_reject.iteritems():
        for pkg in transition['packages']:
            if pkg not in packages:
                packages[pkg] = {}
            if id not in packages[pkg]:
                package[pkg][id] = {}
            package[pkg][id]['reject'] = True
            package[pkg][id]['status'] = 'ongoing'
    for pkgdata in yaml_pkglist:
        pkg = pkgdata['name']
        for t in pkgdata['list']:
            if t[1] not in ('ongoing', 'planned'):
                continue # Skip non-interesting transitions
            if pkg not in packages:
                packages[pkg] = {}
            if t[0] not in packages[pkg]:
                packages[pkg][t[0]] = {}
            packages[pkg][t[0]]['status'] = t[1]
    return packages

def read_piuparts(fname):
    failures = {}
    if os.path.exists(fname):
        f = open(fname)
        for line in f.readlines():
            try:
                src, outcome = map(lambda s: s.strip(), line.split(':'))
                if outcome == "fail":
                    failures[src] = True
            except ValueError:
                pass
        f.close()
    return failures

def read_packages_list(fname):
    failures = {}
    if os.path.exists(fname):
        f = open(fname)
        for line in f.readlines():
            line = line.strip()
            failures[line] = True
        f.close()
    return failures

def read_depneedsmaint(fname):
    outarray = {}
    if os.path.exists(fname):
        f = open(fname)
        for line in f.readlines():
            line = line.strip()
            (pkg, pkg2, deptype, binpkg, wnpptype, wnppbug) = line.split()
            if pkg not in outarray:
                outarray[pkg] = {}
            if pkg2 not in outarray[pkg]:
                outarray[pkg][pkg2] = {}
            outarray[pkg][pkg2]['deptype'] = deptype
            outarray[pkg][pkg2]['binpkg'] = binpkg
            outarray[pkg][pkg2]['wnpptype'] = wnpptype
            outarray[pkg][pkg2]['wnppbug'] = wnppbug
        f.close()
    return outarray

def read_lintian_info(fname):
    lintian = {} # maps source pkg names to lintian complaint counts
    for line in open(fname).readlines():
        try:
            line = line.split()
            lintian[line[0]] = [int(c) for c in line[1:]]
        except ValueError:
            continue
    return lintian

def read_logcheck_info(fname):
    data = {} # maps source pkg names to pairs <errors_no, warnings_no>
    for line in open(fname).readlines():
        try:
            (pkg, errors_no, warnings_no) = line.split('|')[:3]
	    data[pkg] = (int(errors_no), int(warnings_no))
        except ValueError:
            continue
    return data

def read_clang_info(fname):
    clang = {}
    try:
        with open(fname) as f:
            for line in f:
                try:
                    pkg, ver, log, err = line.strip().split(' ', 3)
                except ValueError:
                    continue
                if pkg in clang: continue
                clang[pkg] = log
    except IOError:
        pass
    return clang

# parse textual files with lines like "field: value"
def read_mapping(fname):
    f = open(fname)
    for line in f.readlines():
	items = map(string.strip, line.split(':'))
	if len(items) == 2:
	    yield items
	else:
	    sys.stderr.write("Incorrect 'field: value' data in %s: %s" % (fname, line))
    f.close()

# read 822 dump of NEW queue content
def read_NEW(fname):
    new_contents = {}
    for stanza in deb822.Sources.iter_paragraphs(file(os.path.join(dir, 'new.822'))):
        if stanza.has_key('source') and stanza.has_key('version') and \
                stanza.has_key('queue'):
            # store only the most recent version in NEW (ignore accepted)
            versions = stanza['version'].split()
            versions.sort( lambda a,b: debian_support.version_compare(a,b) )
            version = versions[-1]
            if stanza['queue'] == 'new' and \
                    (not new_contents.has_key(stanza['source']) or \
                     debian_support.version_compare( \
                                    new_contents[stanza['source']],
                                    version) < 0):
                new_contents[stanza['source']] = version
    return new_contents

def read_dm(fname):
    dm_contents = {}
    f = open(fname)
    for stanza in deb822.Deb822.iter_paragraphs(f):
        if stanza.has_key('Uid') and stanza.has_key('Allow'):
            # Allow is a comma-separated string of 'package (DD fpr)' items,
            # where DD fpr is the fingerprint of the DD that granted the
            # permission
            name, email = stanza['Uid'].rsplit(' ', 1)
            email = email.strip('<>')
            for pair in stanza['Allow'].split(','):
                pair = pair.strip()
                pkg, dd_fpr = pair.split()
                pkg = pkg.encode('utf-8')
                dm_contents.setdefault(pkg, [])
                dm_contents[pkg].append({'name': name, 'email': email})
    f.close()
    return dm_contents

def read_l10n_status(fname):
    l10n = {}
    if os.path.exists(fname):
        f = open(fname)
        for line in f.readlines():
            if line.startswith('#'):
                continue
            pkg, version, trans, url, todo = line.rstrip().split()
            deb_trans, nondeb_trans = trans.strip('()').split(',')
            l10n[pkg] = {'version': version,
                         'deb': deb_trans, 'nondeb': nondeb_trans,
                         'url': url, 'todo': todo}
        f.close()
    return l10n

def read_autoremoval_info(fname):
    y = yaml.load(file(fname), yaml.CSafeLoader)
    packages = {} # maps pkg to the font review for it
    if y:
        packages = y
    return packages

# Initialization: fill binary <-> source maps
f = open(odir + "/sources_mapping")
source2binaries = cPickle.load(f) # maps a source package to its binaries
binary2sources = cPickle.load(f)  # maps a binary package to its sources
f.close()

# Read all the bugs stats
bugs = {}
f = open(dir + "/bugs.txt")
while 1:
    line = f.readline()
    if not line: break #eof
    line = line.strip()
    (binary, stats) = line.split(None, 1)
    try:
        bugs[binary] = [ string.atoi(i) for i in stats.split() ]
    except:
        sys.stderr.write("Failed to parse bugs.txt stats for %s: %s\n" % (binary, stats))
f.close()

srcbugs = {}
f = open(dir + "/bugs-src.txt")
while 1:
    line = f.readline()
    if not line: break #eof
    line = line.strip()
    (pkg, stats) = line.rsplit(":", 1)
    try:
        srcbugs[pkg] = [ string.atoi(i) for i in stats.replace("(", " ").replace(")", " ").split() ]
    except:
        sys.stderr.write("Failed to parse bugs-src.txt stats for %s: %s\n" % (pkg, stats))
f.close()

newcomer_bugs = read_bug_summary(os.path.join(dir, 'bugs.newcomer.txt'))
help_bugs = read_bug_summary(os.path.join(dir, 'bugs.help.txt'))

# Read all the PTS stats
pts = {}
f = open(dir + "/count.txt")
while 1:
    line = f.readline()
    if not line: break #eof
    line = line.strip()
    (binary, stats) = line.split(None, 1)
    pts[binary] = stats
f.close()

# Read the lisf of packages with debcheck problems
debcheck = {}
for dist in ("oldstable", "stable", "testing", "unstable"):
    debcheck[dist] = {}
    f = open(dir + "/debcheck-" + dist)
    while 1:
        line = f.readline()
        if not line: break #eof
        debcheck[dist][line.strip()] = 1
    f.close()

# Read the list of packages with override disparities
override = {}
for dist in ("unstable", "experimental"):
    override[dist] = {}
    f = open(dir + "/override-disparities." + dist)
    for line in f:
        if line[0] != '-':
            source = line.strip()[:-1]
            override[dist][source] = []
        else:
            override[dist][source].append(line.strip()[2:])
    f.close()

# read the package localization status
l10n = read_l10n_status(os.path.join(dir, 'l10n-status.txt'))

# Read the current signature of other.xml files
sigs = {}
if os.path.exists(odir + "/other.sigs"):
    f = open(odir + "/other.sigs", "r")
    sigs = cPickle.load(f)
    f.close()

# Read the wnpp information. [PvR]
wnpp = {}
if os.path.exists(dir + "/wnpp_rm"):
    f = open(dir + "/wnpp_rm")
    while 1:
        line = f.readline()
        if not line: break # eof
        line = line.strip()
        try:
            (package, type, number) = line.split("|")[0].split()
        except:
            #too many badly formatted ITP... disable warning. --RH
            #sys.stderr.write("Ignoring bad line '%s' in wnpp_rm\n" % line)
            pass
        wnpp[package[:-1]] = (type, number)
    f.close()

# Read Debian patches information
debian_patches = set()
with open(os.path.join(dir,'patches.debian')) as f:
    data = json.load(f)
    debian_patches = set([i['name'] for i in data['packages']])

# Read derivatives patches information
derivs_patches = set()
with open(os.path.join(dir,'patches.derivs')) as f:
    derivs_patches = set(yaml.safe_load(f).keys())

# Read patches information [FG]
ubuntu_patches = {}
# this can be easily inserted into a new update_patches.py if it becomes too
# heavy to parse the file
if os.path.exists(dir + "/patches.ubuntu"):
    f = open(dir + "/patches.ubuntu")
    for line in f.readlines():
        (package, rel_url) = line.split(' ', 2)
        rel_url = rel_url.strip()
        r = re.search("_(\S+).patch", line)
        if not r:
            continue
        version = r.group(1)
        ubuntu_patches[package] = (version, "https://patches.ubuntu.com/" + rel_url)
    f.close()

ubuntu_versions = {}
if os.path.exists(dir + "/versions.ubuntu"):
    f = open(dir + "/versions.ubuntu")
    for line in f.readlines():
        (package, version) = line.split(' ', 2)
        version = version.strip()
	ubuntu_versions[package] = (version, "https://launchpad.net/ubuntu/+source/" + package)
    f.close()

ubuntu_bugs = {}
ubuntu_bugpatches = {}
if os.path.exists(dir + "/bugs.ubuntu"):
    f = open(dir + "/bugs.ubuntu")
    for line in f.readlines():
        (package, ubugs, upatches) = line.split('|', 3)
        ubugs = ubugs.strip()
        upatches = upatches.strip()
	ubuntu_bugs[package] = (ubugs, "https://bugs.launchpad.net/ubuntu/+source/" + package)
	ubuntu_bugpatches[package] = (upatches, "https://bugs.launchpad.net/ubuntu/+source/" + package + "/+patches")
    f.close()

# write lowThresholdNmu info to a (global, i.e. not per-package) file
#
# XXX this is sub-optimal, as the XSLT rendering of each package page will have
# to read the whole XML file each time. However, this can't be fixed here
# unless we have somewhere an additional map (in the spirit of sources.map)
# mapping source package names to maintainer emails
low_nmu_emails = read_low_threshold_nmu(dir + "/low_threshold_nmu.txt")
f = open(odir + "/low_threshold_nmu.emails.xml", 'w')
f.write("<emails>\n");
f.writelines(map(lambda s: "  <email>%s</email>\n" % s, low_nmu_emails))
f.write("</emails>\n");
f.close()

# read the list of packages involved in transitions
transitions = read_transitions(os.path.join(dir, "transitions.yaml"),
                               os.path.join(dir, "transitions-packages.yaml"))

piuparts = read_piuparts(os.path.join(dir, "piuparts-sid.txt"))

watchbroken = read_packages_list(os.path.join(dir, "watch-broken.txt"))
watchavail = read_packages_list(os.path.join(dir, "watch-avail.txt"))

depneedsmaint = read_depneedsmaint(os.path.join(dir, "depneedsmaint.txt"))

new_queue = read_NEW(os.path.join(dir, "new.822"))
dms = read_dm(os.path.join(dir, 'dm.txt'))

# read QA lintian info
lintian = read_lintian_info(os.path.join(dir, "lintian.qa-list.txt"))

# read builddlogcheck info
logcheck = read_logcheck_info(os.path.join(dir, "logcheck.txt"))

# read clang build info
clang = read_clang_info(os.path.join(dir, "clang.txt"))

# read autoremoval info
autoremovals = read_autoremoval_info(os.path.join(dir, "autoremovals.yaml"))

# read as-installed testing info
testing = read_testing_info(os.path.join(dir, "testing.json"))

# read the list of packages indexed by svnbuildstat.debian.net
svnbuildstat = {}
try:
    f = open(os.path.join(dir, "svnbuildstat_list.txt"))
    for pkgname in map(string.rstrip, f.readlines()):
        svnbuildstat[pkgname] = True
    f.close()
except:
    pass # Silent failure

# read info gathered from watch file scanner
with open(os.path.join(dir, "dehs.json")) as data_file:
    data = json.load(data_file)
if not data:
    data = []
dehs = {}
for entry in data:
    pkgname = entry["package"]
    if "status" in entry and "newer package" in entry["status"]:
        if pkgname not in dehs:
            dehs[pkgname] = {}
        dehs[pkgname]["newer"] = str(entry["upstream-version"])
        dehs[pkgname]["url"] = entry["upstream-url"]
    if entry["warnings"] and "more than one main upstream tarballs listed." not in entry["warnings"]:
        if pkgname not in dehs:
            dehs[pkgname] = {}
        dehs[pkgname]["error"] = entry["warnings"]

# read list of unfixed security issues
security = {}
for pkgname, count in read_mapping(os.path.join(dir, "security_issues.txt")):
    security[pkgname] = count

# read short descriptions
shortdescs = read_shortdesc(os.path.join(dir, "shortdesc.txt"))

# read short descriptions
longdescs = read_longdesc(os.path.join(dir, "longdesc.json"))

font_reviews = read_font_reviews(os.path.join(dir, "debian-font-review.yaml"))

url_issues = read_duck(os.path.join(dir, "duck.txt"))

screenshots_packages = read_screenshots(os.path.join(dir, "screenshots-packages.json"))

dedup_packages = read_dedup(os.path.join(dir, "dedup.txt"))

upstream_info_packages = read_upstream_info(os.path.join(dir, "upstream-info.txt"))

# read release goals information
data = yaml.load(file(os.path.join(dir, "release-goals.yaml")), yaml.CSafeLoader)
release_goals = {}
if data and "release-goals" in data and data["release-goals"]:
    for goal in data["release-goals"]:
        if "bugs" in goal:
            for tag in goal["bugs"]["usertags"]:
                release_goals["%s %s" % (goal["bugs"]["user"], tag)] = goal
data = yaml.load(file(os.path.join(dir, "release-goals-bugs.yaml")), yaml.CSafeLoader)
rg_bugs = {}
rg_bugs_done = {}
if data:
  for bug in data:
    goal = "%s %s" % (bug["email"], bug["tag"])
    if goal not in release_goals:
        continue
    rg_info = release_goals[goal]
    if rg_info["state"] != "accepted":
        continue
    if str(bug["id"]) in rg_bugs_done: # Skip duplicates
        continue
    if bug["source"] not in rg_bugs:
        rg_bugs[bug["source"]] = []
    rg_bugs[bug["source"]].append({
        "name": rg_info["name"],
        "url": rg_info["url"],
        "id": str(bug["id"])
    })
    rg_bugs_done[str(bug["id"])] = True

# Create the XML documents
while 1:
    line = sys.stdin.readline()
    if not line: break #eof
    pkg = line.strip()

    doc = xml.dom.getDOMImplementation('minidom').createDocument(None, "other", None)
    root_elt = doc.documentElement
    hash = hash_name(pkg)

    # Add debcheck availability info
    dc_sig = ""
    elt = doc.createElement("debcheck")
    for dist in ("oldstable", "stable", "testing", "unstable"):
        if debcheck[dist].has_key(pkg):
            elt.setAttribute(dist, "yes")
            dc_sig += "y"
        else:
            elt.setAttribute(dist, "no")
            dc_sig += "n"
    root_elt.appendChild(elt)

    # Add NEW queue versions, if any
    if new_queue.has_key(pkg):
        new_version = new_queue[pkg]
        new_version_upstream = re.sub(r'-[^-]*$', '', new_version)
        new_version_upstream = re.sub(r'^[0-9]+:', '', new_version_upstream)
        root_elt.setAttribute("new_version", new_version)
        root_elt.setAttribute("new_version_upstream", new_version_upstream)
        new_queue_sig = new_version
    else:
        new_queue_sig = ''

    # Add DM information
    dms_sig = []
    elt = doc.createElement('dms')
    for dm in dms.get(pkg, []):
        sub_elt = doc.createElement('item')
        sub_elt.setAttribute('name', dm['name'])
        sub_elt.setAttribute('email', dm['email'])
        elt.appendChild(sub_elt)
        dms_sig.append(dm)
    root_elt.appendChild(elt)

    # Get PTS stats
    elt = doc.createElement("pts")
    elt.setAttribute("count", pts.get(pkg, "0"))
    root_elt.appendChild(elt)

    # Get BTS stats
    elt = doc.createElement("bugs")
    (s_rc, s_rc_m, s_normal, s_normal_m, s_wishlist, s_wishlist_m, s_fixed,
            s_fixed_m, s_patch, s_patch_m) = \
                    srcbugs.get(pkg, [0,0,0,0,0,0,0,0,0,0])
    try:
        binlist = source2binaries[pkg]['merged']
    except:
        binlist = []
    binlist.sort()
    subsig = ""
    for binary in binlist:
        sub_elt = doc.createElement("item")
        sub_elt.setAttribute("name", binary)
        (rc, normal, wishlist, fixed, patch) = bugs.get(binary, [0,0,0,0,0])
        sub_elt.setAttribute("rc", "%d" % rc)
        sub_elt.setAttribute("normal", "%d" % normal)
        sub_elt.setAttribute("wishlist", "%d" % wishlist)
        sub_elt.setAttribute("fixed", "%d" % fixed)
        sub_elt.setAttribute("patch", "%d" % patch)
        all = rc + normal + wishlist + fixed
        sub_elt.setAttribute("all", "%d" % all)
        elt.appendChild(sub_elt)
        if len(subsig):
            subsig = "%s|%d|%d" % (subsig, all, patch)
        else:
            subsig = "%d|%d" % (all, patch)

    elt.setAttribute("rc", "%d" % s_rc)
    if s_rc != s_rc_m:
        elt.setAttribute("rc_m", "%d" % s_rc_m)
    elt.setAttribute("normal", "%d" % s_normal)
    if s_normal != s_normal_m:
        elt.setAttribute("normal_m", "%d" % s_normal_m)
    elt.setAttribute("wishlist", "%d" % s_wishlist)
    if s_wishlist != s_wishlist_m:
        elt.setAttribute("wishlist_m", "%d" % s_wishlist_m)
    elt.setAttribute("fixed", "%d" % s_fixed)
    if s_fixed != s_fixed_m:
        elt.setAttribute("fixed_m", "%d" % s_fixed_m)
    elt.setAttribute("patch", "%d" % s_patch)
    if s_patch != s_patch_m:
        elt.setAttribute("patch_m", "%d" % s_patch_m)
    s_all = s_fixed + s_wishlist + s_normal + s_rc
    s_all_m = s_fixed_m + s_wishlist_m + s_normal_m + s_rc_m
    elt.setAttribute("all", "%d" % s_all)
    if s_all != s_all_m:
        elt.setAttribute("all_m", "%d" % s_all_m)
    root_elt.appendChild(elt)
    if newcomer_bugs.has_key(pkg):
        s_newcomer = newcomer_bugs[pkg]
    else:
        s_newcomer = 0
    elt.setAttribute("newcomer", str(s_newcomer))
    if help_bugs.has_key(pkg):
        s_help = help_bugs[pkg]
    else:
        s_help = 0
    elt.setAttribute("help", str(s_help))

    # Get WNPP information. [PvR]
    if wnpp.has_key(pkg):
        (type, number) = wnpp[pkg]
        elt = doc.createElement("wnpp")
        elt.setAttribute("type", type)
        elt.setAttribute("bugnumber", number)
        root_elt.appendChild(elt)
        root_elt.setAttribute("wnpp", "yes")
        wnpp_sig = "%s%s" % (type,number)
    else:
        root_elt.setAttribute("wnpp", "no")
        wnpp_sig = "n"

    # Get override info [JvW]
    override_elt = None
    override_sig = []
    for dist in [ 'unstable', 'experimental' ]:
        if override[dist].has_key(pkg):
            if not override_elt: override_elt = doc.createElement("override")
            disparities = override[dist][pkg]
            override_sig.append(disparities)
            elt_g = doc.createElement("group")
            elt_g.setAttribute("suite", dist)
            for disp in disparities:
                elt = doc.createTextNode(disp)
                elt_disp = doc.createElement("disparity")
                elt_disp.appendChild(elt)
                elt_g.appendChild(elt_disp)
            override_elt.appendChild(elt_g)

    if override_elt:
        root_elt.appendChild(override_elt)
        root_elt.setAttribute("override", "yes")
    else:
        root_elt.setAttribute("override", "no")

    # Add Debian patches information
    if pkg in debian_patches:
        root_elt.setAttribute("patches", "yes")
        patches_sig = "y"
    else:
        root_elt.setAttribute("patches", "no")
        patches_sig = "n"

    # Add derivatives patches information
    if pkg in derivs_patches:
        root_elt.setAttribute("derivs", "yes")
        derivs_sig = "y"
    else:
        root_elt.setAttribute("derivs", "no")
        derivs_sig = "n"

    # Add Ubuntu information
    if ubuntu_versions.has_key(pkg):
        elt = doc.createElement("ubuntu")
        (version, url) = ubuntu_versions[pkg]
	ubuntu_sig = "ubuntu/ " + version
        elt.setAttribute("version", unicode(version, 'UTF8', 'replace'))
        elt.setAttribute("url", unicode(url, 'UTF8', 'replace'))
    	if ubuntu_bugs.has_key(pkg):
           elt.setAttribute("bugs", "yes")
           elt_bugs = doc.createElement("bugs")
           (count, url) = ubuntu_bugs[pkg]
           ubuntu_sig += "ubuntubugs/" + count
           elt_bugs.setAttribute("count", unicode(count, 'UTF8', 'replace'))
           elt_bugs.setAttribute("url", unicode(url, 'UTF8', 'replace'))
           elt.appendChild(elt_bugs)
       	   if ubuntu_bugpatches[pkg][0] != "0":
              elt.setAttribute("bugpatches", "yes")
              elt_bugpatches = doc.createElement("bugpatches")
              (count, url) = ubuntu_bugpatches[pkg]
              ubuntu_sig += "ubuntubugpatches/" + count
              elt_bugpatches.setAttribute("count", unicode(count, 'UTF8', 'replace'))
              elt_bugpatches.setAttribute("url", unicode(url, 'UTF8', 'replace'))
              elt.appendChild(elt_bugpatches)
	if ubuntu_patches.has_key(pkg):
           elt.setAttribute("patch", "yes")
           elt_patch = doc.createElement("patch")
           (version, url) = ubuntu_patches[pkg]
           ubuntu_sig += "ubuntupatch/" + version
           elt_patch.setAttribute("version", unicode(version, 'UTF8', 'replace'))
           elt_patch.setAttribute("url", unicode(url, 'UTF8', 'replace'))
           elt.appendChild(elt_patch)
        root_elt.appendChild(elt)
        root_elt.setAttribute("ubuntu", "yes")
    else:
        root_elt.setAttribute("ubuntu", "no")
        ubuntu_sig = "n"

    # Get DEHS information
    if dehs.has_key(pkg):
        elt = doc.createElement('dehs')
        root_elt.appendChild(elt)
        root_elt.setAttribute('dehs', 'yes')
        dehs_sig = ''
        if dehs[pkg].has_key('newer'):
            elt.setAttribute('newer', dehs[pkg]['newer'])
            elt.setAttribute('url', dehs[pkg]['url'])
            dehs_sig += "newer/" + dehs[pkg]['newer']
        if dehs[pkg].has_key('error'):
            elt.setAttribute('error', dehs[pkg]['error'])
            dehs_sig += "error"
    else:
        root_elt.setAttribute('dehs', 'no')
        dehs_sig = 'n'

    # add svnbuildstat info
    if svnbuildstat.has_key(pkg):
        elt = doc.createElement("svnbuildstat")
        root_elt.setAttribute("svnbuildstat", "yes")
        root_elt.appendChild(elt)
        svnbuildstat_sig = "y"
    else:
        root_elt.setAttribute("svnbuildstat", "no")
        svnbuildstat_sig = "n"

    # add piuparts info
    if piuparts.has_key(pkg):
        root_elt.setAttribute("piuparts", "yes")
        piuparts_sig = "y"
    else:
        root_elt.setAttribute("piuparts", "no")
        piuparts_sig = "n"

    # add watchbroken info
    if watchbroken.has_key(pkg):
        root_elt.setAttribute("watchbroken", "yes")
        watchbroken_sig = "y"
    else:
        root_elt.setAttribute("watchbroken", "no")
        watchbroken_sig = "n"

    # add watchavail info
    if watchavail.has_key(pkg):
        root_elt.setAttribute("watchavail", "yes")
        watchavail_sig = "y"
    else:
        root_elt.setAttribute("watchavail", "no")
        watchavail_sig = "n"

    # add localization info
    if l10n.has_key(pkg):
        elt = doc.createElement("i18n")
        deb, nondeb = l10n[pkg]['deb'], l10n[pkg]['nondeb']
        elt.setAttribute('deb', deb)
        elt.setAttribute('nondeb', nondeb)
        if not (set([deb, nondeb]) <= set(['-', '100'])):
            # do not show l10n status when fully translated or nothing
            # is translatable
            elt.setAttribute('href', l10n[pkg]['url'])
        if l10n[pkg]['todo'] == '1':
            elt.setAttribute('todo', 'yes')
        root_elt.setAttribute("i18n", "yes")
        root_elt.appendChild(elt)
        i18n_sig = (deb, nondeb, l10n[pkg]['todo'])
    else:
        root_elt.setAttribute("i18n", "no")
        i18n_sig = ('-','-', 0)

    # add transitions info
    if transitions.has_key(pkg):
        elt = doc.createElement("transitions")
        transitions_sig = ""
        for name, data in transitions[pkg].iteritems():
            trans_elt = doc.createElement("transition")
            trans_elt.setAttribute("name", name)
            trans_elt.setAttribute("status", data.get('status', 'unknown'))
            if data.get('reject', False):
                trans_elt.setAttribute("reject", 'yes')
            else:
                trans_elt.setAttribute("reject", 'no')
            elt.appendChild(trans_elt)
            transitions_sig += "%s/%s " % (name, data['status'])
        root_elt.setAttribute("transitions", "yes")
        root_elt.appendChild(elt)
    else:
        root_elt.setAttribute("transitions", "no")
        transitions_sig = "n"

    # add release goals info
    if rg_bugs.has_key(pkg):
        elt = doc.createElement("releasegoals")
        rg_sig = ""
        for bug in rg_bugs[pkg]:
            rg_elt = doc.createElement("bug")
            rg_elt.setAttribute("id", bug["id"])
            rg_elt.setAttribute("name", bug["name"])
            rg_elt.setAttribute("url", bug["url"])
            elt.appendChild(rg_elt)
            rg_sig += "%s " % bug["id"]
        root_elt.setAttribute("releasegoals", "yes")
        root_elt.appendChild(elt)
    else:
        root_elt.setAttribute("releasegoals", "no")
        rg_sig = "n"

    # add depneedsmaint info
    if depneedsmaint.has_key(pkg):
        elt = doc.createElement("depneedsmaint")
        depneedsmaint_sig = ""
        for pkg2 in depneedsmaint[pkg]:
            elt2 = doc.createElement("package")
            elt2.setAttribute("name", pkg2)
            elt2.setAttribute("deptype", depneedsmaint[pkg][pkg2]['deptype'])
            elt2.setAttribute("binpkg", depneedsmaint[pkg][pkg2]['binpkg'])
            elt2.setAttribute("wnpptype", depneedsmaint[pkg][pkg2]['wnpptype'])
            elt2.setAttribute("wnppbug", depneedsmaint[pkg][pkg2]['wnppbug'])
            elt.appendChild(elt2)
            depneedsmaint_sig += "%s " % pkg2
        root_elt.setAttribute("depneedsmaint", "yes")
        root_elt.appendChild(elt)
    else:
        root_elt.setAttribute("depneedsmaint", "no")
        depneedsmaint_sig = "n"

    # add lintian QA info
    if lintian.has_key(pkg):
        elt = doc.createElement("lintian")
        for index, name in enumerate(("errors", "warnings","pedantics","experimentals","overriddens")):
            elt.setAttribute(name, str(lintian[pkg][index]))
        root_elt.appendChild(elt)
        if sum(lintian[pkg]) > 0:
            root_elt.setAttribute("lintian", "yes")
        else:
            root_elt.setAttribute("lintian", "no")
        lintian_sig = lintian[pkg][0:2]
    else:
        root_elt.setAttribute("lintian", "no")
        lintian_sig = (0, 0)

    # add logcheck info
    if logcheck.has_key(pkg):
        (errs, warns) = logcheck[pkg]
        elt = doc.createElement("logcheck")
        elt.setAttribute("errors", str(errs))
        elt.setAttribute("warnings", str(warns))
        root_elt.appendChild(elt)
        root_elt.setAttribute("logcheck", "yes")
        logcheck_sig = (errs, warns)
    else:
        root_elt.setAttribute("logcheck", "no")
        logcheck_sig = (0, 0)

    # add clang info
    if clang.has_key(pkg):
        elt = doc.createElement("clang")
        elt.setAttribute('href', clang[pkg])
        root_elt.setAttribute("clang", "yes")
        root_elt.appendChild(elt)
        clang_sig = clang[pkg]
    else:
        root_elt.setAttribute("clang", "no")
        clang_sig = ''

    # add short descriptions
    elt = doc.createElement("descriptions")
    root_elt.appendChild(elt)
    if shortdescs.has_key(pkg):
        for package, shortdesc in shortdescs[pkg].iteritems():
            desc_elt = doc.createElement("shortdesc")
            elt.appendChild(desc_elt)
            desc_elt.setAttribute("package", package)
            desc_text = doc.createTextNode(unicode(shortdesc, "UTF-8", "replace"))
            desc_elt.appendChild(desc_text)
        shortdesc_sig = str(shortdescs[pkg]).__hash__()
            # XXX hash(str(...)) does not work: WTF?
    else:
        shortdesc_sig = ''.__hash__()

    # add long descriptions
    if longdescs.has_key(pkg):
        for package, longdesc in longdescs[pkg].iteritems():
            desc_elt = doc.createElement("longdesc")
            elt.appendChild(desc_elt)
            desc_elt.setAttribute("package", package)
            desc_text = doc.createTextNode(longdesc)
            desc_elt.appendChild(desc_text)
        longdesc_sig = str(longdescs[pkg]).__hash__()
            # XXX hash(str(...)) does not work: WTF?
    else:
        longdesc_sig = ''.__hash__()

    # Add font review links
    if font_reviews.has_key(pkg):
        elt = doc.createElement("fonts")
        elt.setAttribute('href', font_reviews[pkg])
        root_elt.setAttribute("fonts", "yes")
        root_elt.appendChild(elt)
        fonts_sig = font_reviews[pkg]
    else:
        root_elt.setAttribute("fonts", "no")
        fonts_sig = ''

    # Add URL check links
    if pkg in url_issues:
        root_elt.setAttribute("urlissues", "yes")
        url_issues_sig = 'y'
    else:
        root_elt.setAttribute("urlissues", "no")
        url_issues_sig = 'n'

    # Add screenshots links
    if pkg in screenshots_packages:
        elt = doc.createElement("screenshots")
        root_elt.appendChild(elt)
        for package in screenshots_packages[pkg]:
                pkg_elt = doc.createElement("package")
                pkg_elt.setAttribute("name", package)
                elt.appendChild(pkg_elt)
        screenshots_sig = screenshots_packages[pkg]
    else:
        root_elt.setAttribute("screenshots", "no")
        screenshots_sig = ''

    if pkg in dedup_packages:
        units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
        elt = doc.createElement("dedup")
        root_elt.appendChild(elt)
        root_elt.setAttribute("dedup", "yes")
        for size, pkg_a, pkg_b in dedup_packages[pkg]:
            size = int(size)
            for unit in units:
                if size < 1024.0:
                    size = "%3.1f %s" % (size, unit)
                    break
                size /= 1024.0
            dedup_elt = doc.createElement("item")
            dedup_elt.setAttribute("size", size)
            dedup_elt.setAttribute("pkg_a", pkg_a)
            dedup_elt.setAttribute("pkg_b", pkg_b)
            elt.appendChild(dedup_elt)
        dedup_sig = dedup_packages[pkg]
    else:
        root_elt.setAttribute("dedup", "no")
        dedup_sig = ''

    # Add upstream info links
    if pkg in upstream_info_packages:
        root_elt.setAttribute("upstreaminfo", "yes")
        upstream_info_sig = 'y'
    else:
        root_elt.setAttribute("upstreaminfo", "no")
        upstream_info_sig = 'n'


    # Get security issues
    if security.has_key(pkg):
        root_elt.setAttribute('security', security[pkg])
        sec_sig = security[pkg]
    else:
        root_elt.setAttribute('security', '0')
        sec_sig = '0'

    # add autoremoval info
    if autoremovals.has_key(pkg):
        autoremoval_info = autoremovals[pkg]
        elt = doc.createElement("autoremoval")
        if len(autoremoval_info["bugs"]) > 0:
            elt.setAttribute("bugs","yes")
            for bug in autoremoval_info["bugs"]:
                bug_elt = doc.createElement("bug")
                bug_elt.setAttribute("bugid",bug)
                elt.appendChild(bug_elt)
        if autoremoval_info.has_key("bugs_dependencies"):
            elt.setAttribute("bugs_dep","yes")
            for bug in autoremoval_info["bugs_dependencies"]:
                bug_elt = doc.createElement("bug_dep")
                bug_elt.setAttribute("bugid",bug)
                elt.appendChild(bug_elt)
        if autoremoval_info.has_key("buggy_dependencies"):
            elt.setAttribute("buggy_dep","yes")
            for dep in autoremoval_info["buggy_dependencies"]:
                bug_elt = doc.createElement("buggy_dep")
                bug_elt.setAttribute("pkg",dep)
                elt.appendChild(bug_elt)
        if autoremoval_info.has_key("rdeps"):
            elt.setAttribute("rdep","yes")
            for rdep in autoremoval_info["rdeps"]:
                bug_elt = doc.createElement("rdep")
                bug_elt.setAttribute("pkg",rdep)
                elt.appendChild(bug_elt)
        removaldate = "on " + autoremoval_info["removal_date"].strftime("%Y-%m-%d")
        if autoremoval_info["removal_date"] < datetime.now():
            removaldate = "today"

        elt.setAttribute("removaldate",removaldate)
        elt.setAttribute("version",autoremoval_info["version"])

        root_elt.appendChild(elt)
        root_elt.setAttribute("autoremoval", "yes")
        autoremoval_sig = autoremoval_info
    else:
        root_elt.setAttribute("autoremoval", "no")
        autoremoval_sig = "n"

    # add as-installed testing info
    if testing.has_key(pkg):
        elt = doc.createElement("testing")
        elt.setAttribute('status', str(testing[pkg]))
        root_elt.appendChild(elt)
        root_elt.setAttribute("testing", "yes")
        testing_sig = testing[pkg]
    else:
        root_elt.setAttribute("testing", "no")
        testing_sig = ''

    # TODO: try to do that signature checking before the creation of XML DOM
    # Build the sig and check if anything changed
    sig = (pts.get(pkg, "0"), dc_sig, wnpp_sig, override_sig, dehs_sig,
            ubuntu_sig, s_rc, s_normal, s_wishlist, s_fixed, s_newcomer, s_help,
            subsig, svnbuildstat_sig, transitions_sig, lintian_sig,
            shortdesc_sig, piuparts_sig, new_queue_sig, i18n_sig,
            watchbroken_sig, watchavail_sig, depneedsmaint_sig, dms_sig,
            fonts_sig, sec_sig, logcheck_sig, rg_sig, url_issues_sig,
            screenshots_sig, clang_sig, dedup_sig, longdesc_sig,
            upstream_info_sig, autoremoval_sig, testing_sig,
            patches_sig, derivs_sig)
    if sigs.has_key(pkg) and sig == sigs[pkg] and \
            os.path.isfile("%s/%s/%s/other.xml" % (odir, hash, pkg)):
        continue
    sigs[pkg] = sig

    # Output the data to the XML file
    try:
        f = open("%s/%s/%s/other.xml" % (odir, hash, pkg), "w")
        f.write(doc.toxml(encoding="UTF-8"))
        f.close()
    except Exception, msg:
        sys.stderr.write("Output problem for " + pkg + "/other.xml (%s)\n" %
                msg);

# Store the signatures
f = open(odir + "/other.sigs", "w")
cPickle.dump(sigs, f, 0)
f.close()

