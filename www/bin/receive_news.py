#!/usr/bin/python
# -*- coding: utf-8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

# Copyright 2002 Raphaël Hertzog
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import os, os.path, sys, email, common

from config import dir, odir, root
from common import hash_name

os.nice(19)
os.umask(0002)
os.environ["PATH"] = "/bin:/sbin:/usr/bin:/usr/sbin"

msg = email.message_from_file(sys.stdin)

info = common.extract_info(msg)

subdir = "news"
if info.has_key("static") or (len(sys.argv) >= 2 and sys.argv[1] == "static"):
    subdir = "static"

if info.has_key("package"):
    pkg = info["package"]
    hash = hash_name(pkg)
    srcdir = "%s/base/%s/%s" % (root, hash, pkg)
    if not os.path.isdir(srcdir):
        sys.exit("Invalid source package: %s" % pkg)
    outdir = srcdir + "/" + subdir
    htmldir = "%s/web/%s/%s/%s" % (root, hash, pkg, subdir)
    common.save_msg_in_dir(msg, outdir)
    # Synchronize the XML document
    f = os.popen("%s/bin/update_news.py" % root, "w")
    f.write(pkg+"\n")
    f.close()
    # Regenerate the HTML page
    f = os.popen("%s/bin/generate_html.sh" % root, "w")
    f.write(pkg+"\n")
    f.close()
else:
    # We should give a meaningful error here but we don't
    # because it generates backscatter on the spam received
    # on the aliases
    sys.exit(0)
