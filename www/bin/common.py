# -*- coding: utf-8 -*-
# vim: expandtab

# Copyright 2002,2009 Raphaël Hertzog
# Copyright 2006 Jeroen van Wolffelaar
# Copyright 2007-2008 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import hashlib, os, os.path, re, rfc822, time, email, sys
from email import Utils, Header

from config import root

def save_msg_in_dir(msg, dir):
    """Add the message in the directory."""
    if not os.path.isdir(dir):
        os.makedirs(dir)
    info = extract_info(msg)

    nonce = 0
    targetfile = "%s/%s.%d.txt" % (dir, info['timestamp'], nonce)
    while os.path.isfile(targetfile):
        nonce += 1
        if nonce > 128: # eventually give up
            raise("can't find a free slot to save message, last tried was %s"\
                    % targetfile)
        targetfile = "%s/%s.%d.txt" % (dir, info['timestamp'], nonce)
    f = open(targetfile, "w")
    f.write(msg.as_string())
    f.close()
    os.chmod(targetfile, 0664)

re_katie_install = re.compile(r"^\S+ \S+ (\S+)")
re_distribution = re.compile(r"^Distribution:\s*(\S+)", re.M)
re_urgency = re.compile(r"^Urgency:\s*(\S+)", re.M)

def extract_info(msg):
    """Extract pseudo-header informations in a dictionnary"""
    info = {}
    if msg.is_multipart():
        for part in msg.walk():
            if part.get_content_type() == "text/plain":
                body = part.get_payload(None, 1)
                break
    else:
        body = msg.get_payload(None, 1)
    lines = body.split("\n", 3)
    for i in range(3):
        res = re.match(r"^(\w+): (\S+)(.*)", lines[i])
        if res:
            field = res.group(1).lower()
            info[field] = unicode(res.group(2), "UTF-8", "replace")
            if field == "subject":
                info[field] = info[field] + unicode(res.group(3), "UTF-8", "replace")
        else:
            break

    if not info.has_key("subject"):
        subject = decode_header(msg.get("subject"))
        subject = re.sub(r"\n\s", " ", subject)
        if subject.split()[0] in ['Accepted', 'Installed']:
            # katie install mail
            version = re_katie_install.search(subject)
            if version:
                version = version.group(1)
            else:
                sys.stderr.write("Can't extract version from subject: %s\n" % subject)
                version = "unknown"
            distribution = re_distribution.search(body)
            if distribution:
                distribution = " in %s" % distribution.group(1)
            else:
                distribution = ""
            urgency = re_urgency.search(body)
            if urgency:
                urgency = " (%s)" % urgency.group(1)
            else:
                urgency = ""
            subject = "Accepted %s%s%s" % (version, distribution, urgency)
        info["subject"] = subject
    if msg.has_key("X-PTS-Subject"):
        info["subject"] = unicode(msg.get("X-PTS-Subject"), "UTF-8", "replace")
    if msg.has_key("X-PTS-Url"):
        info["url"] = msg.get("X-PTS-Url")
    if msg.has_key("X-PTS-Package"):
        info["package"] = msg.get("X-PTS-Package")
    if msg.has_key("date"):
        date = email.Utils.mktime_tz(email.Utils.parsedate_tz(msg.get("date")))
        info["timestamp"] = time.strftime("%Y%m%dT%H%M%SZ", time.gmtime(date))
        info["date"] = time.strftime("%Y-%m-%d", time.gmtime(date))
    if msg.has_key("From"):
        frm = msg.get("From")
        if msg.has_key("X-PTS-From"): frm = msg.get("X-PTS-From")
        (realname, address) = rfc822.parseaddr(frm)
        if realname:
            frm = realname
        else:
            frm = address
        frm = decode_header(frm)
        info["from_name"] = frm
    return info

def hash_name(pkg):
    """returns the hash for package name used in base/ directory"""
    try:
        h = pkg[0]
        if pkg[:3] == "lib":
            h = pkg[:4]
        return h
    except IndexError:
        return ValueError("PTS internal error: 0-length (source) package name")

def store_news(pkg, msg):
    hash = hash_name(pkg)
    basedir = "%s/base/%s/%s" % (root, hash, pkg)
    save_msg_in_dir(msg, basedir+"/news")
    # Synchronize the XML document
    f = os.popen("%s/bin/update_news.py" % root, "w")
    f.write(pkg+"\n")
    f.close()
    # Regenerate the HTML page
    f = os.popen("%s/bin/generate_html.sh" % root, "w")
    f.write(pkg+"\n")
    f.close()

def decode_header(str):
    list = Header.decode_header(str)
    result = u""
    for item in list:
        if item[1] is None:
            result = result + unicode(item[0], "UTF-8", "replace")
        else:
            result = result + item[0].decode(item[1], "replace")
    return result

def ensure_utf8(str):
    try:
        str.decode("utf8")
    except UnicodeError:
        str = str.decode("latin1")
    return str

def md5sum_file(fname):
    """returns md5sum hash in hex for the given file"""
    f = open(fname,"r")
    if not f: return 0

    hash = hashlib.md5()
    for line in f.readlines():
        hash.update(line)
    f.close()
    
    return hash.hexdigest()

# Version Control Systems table
# Fields:
#   'tag' is as it would appear in a vcs-XXX field, e.g. 'svn' for Vcs-Svn
#       tag         common name     upstream URL 
vcs_table = {
        'arch':     ('Arch',        'https://www.gnu.org/software/gnu-arch/'),
        'bzr':      ('Bazaar',      'http://bazaar.canonical.com/'),
        'cvs':      ('CVS',         'http://cvs.nongnu.org/'),
        'darcs':    ('Darcs',       'http://darcs.net/'),
        'git':      ('Git',         'http://git-scm.com/'),
        'hg':       ('Mercurial',   'http://mercurial.selenic.com/'),
        'mtn':      ('Monotone',    'http://www.monotone.ca/'),
        'svn':      ('Subversion',  'https://subversion.apache.org/'),
        }

def dpkg_version_cmp_string(va, vb):
    def dpkg_order(x):
        if x == '~':
            return -1
        elif re.match("\d", x):
            return int(x) + 1
        elif re.match("[A-Za-z]", x):
            return ord(x)
        else:
            return ord(x) + 256
    la = [ dpkg_order(x) for x in "%s" % va ]
    lb = [ dpkg_order(x) for x in "%s" % vb ]
    while True:
        if len(la) == 0 and len(lb) == 0:
            return 0
        a = 0
        b = 0
        if len(la):
            a = la.pop(0)
        if len(lb):
            b = lb.pop(0)
        if a > b:
            return 1
        if a < b:
            return -1

def dpkg_version_cmp_part(va, vb):
    la = re.findall("\d+|\D+", "%s" % va)
    lb = re.findall("\d+|\D+", "%s" % vb)
    while True:
        if len(la) == 0 and len(lb) == 0:
            return 0
        a = "0"
        b = "0"
        if len(la):
            a = la.pop(0)
        if len(lb):
            b = lb.pop(0)
        if re.match("\d+", a) and re.match("\d+", b):
            a = int(a)
            b = int(b)
            res = cmp(a, b)
            if res != 0:
                return res
        else:
            res = dpkg_version_cmp_string(a, b)
            if res != 0:
                return res

class DpkgVersion:
    def __init__(self, version):
        result = re.match("(\d*):(.+)", version)
        if result is not None:
            self.epoch = int(result.group(1))
            version = result.group(2)
            self.no_epoch = False
        else:
            self.epoch = 0
            self.no_epoch = True
        result = re.match("(.+)-(.*)", version)
        if result is not None:
            self.version = result.group(1)
            self.revision = result.group(2)
            self.no_revision = False
        else:
            self.version = version
            self.revision = "0"
            self.no_revision = True

    def __str__(self):
        version = ""
        if not self.no_epoch:
            version += self.epoch + ":"
        version += self.version
        if not self.no_revision:
            version += "-" + self.revision
        return version

    def __repr__(self):
        return "<DpkgVersion " + self.__str__() + ">"

    def __cmp__(self, other):
        res = cmp(self.epoch, other.epoch)
        if res != 0:
            return res
        res = dpkg_version_cmp_part(self.version, other.version)
        if res != 0:
            return res
        return dpkg_version_cmp_part(self.revision, other.revision)

