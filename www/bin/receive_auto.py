#!/usr/bin/python
# -*- coding: utf8 -*-

# Make sure tabs expand to 8 spaces in vim
# vim: expandtab

# Copyright 2002 Raphaël Hertzog
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

import os, os.path, sys, email, common, re

from config import dir, odir, root

os.nice(19)
os.umask(0002)
os.environ["PATH"] = "/bin:/sbin:/usr/bin:/usr/sbin"

#parser = email.Parser.Parser()
msg = email.message_from_file(sys.stdin)

subject = msg.get("subject")
if subject == None:
    sys.exit(0)
words = subject.split()

if len(words) > 1 and (words[0] == "Accepted" or words[0] == "Installed"):
    if msg.get("subject").find("source") == -1: 
        sys.exit(0) # I only want source uploads
    pkg = words[1]
    common.store_news(pkg, msg)
elif msg.has_key("X-DAK"):
    katie = msg.get("X-DAK").split()[1]
    if katie == "rm":
        subject = msg.get("Subject")

        msg["X-PTS-From"] = msg["Sender"]

        # parse what source packages have been removed
        body = msg.get_payload()
        re_rmline = re.compile(r"^\s*(\S+)\s*\|\s*(\S+)\s*\|.*source", re.M)
        source_removals = re_rmline.findall(body)
        suite = re.search(r"have been removed from (\S+):", body).group(1)

        for removal in source_removals:
            pkg = removal[0]
            version = removal[1]
            msg["X-PTS-Subject"] = "Removed %s from %s" % (version, suite)
            common.store_news(pkg, msg)
    else:
        sys.exit("I only understand 'dak rm' mails at this address")
elif msg.has_key("X-Testing-Watch-Package"):
    msg["X-PTS-From"] = "Britney"
    pkg = msg.get("X-Testing-Watch-Package")
    common.store_news(pkg, msg)
elif msg.has_key("X-Mailing-List"):
    sys.exit(0) # silent drop
else:
    sys.exit("This email address only accepts katie's mails.")

