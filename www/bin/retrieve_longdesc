#!/usr/bin/python

# Copyright 2013 Paul Wise <pabs@debian.org>

# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.

# Retrieve most recent package long descriptions from UDD
# Dump the results to stdout as JSON
# This script is used as a QA cronjob, see /srv/qa.debian.org/data/cronjobs/

import sys
import psycopg2
try: import cjson as json
except ImportError: import json

conn = psycopg2.connect("service=udd")
cur = conn.cursor()
cur.execute("""
SELECT v1.package, v1.release, v1.long_description
    FROM (
        SELECT d1.package, d1.release, d1.long_description,
            ROW_NUMBER() OVER (
                PARTITION BY d1.package 
                ORDER BY r1.sort DESC, r1.release, d1.release, d1.package
                ) AS rnum
            FROM descriptions AS d1
            JOIN releases AS r1
                ON d1.release = r1.release
            WHERE d1.language = 'en'
        ) AS v1
    WHERE v1.rnum = 1;
""")
descriptions = cur.fetchall()
descriptions = dict([(package, description.strip()) for (package, release, description) in descriptions])
json.dump(descriptions, sys.stdout)
cur.close()
conn.close()
