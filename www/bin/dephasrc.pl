#!/usr/bin/perl -w

# Copyright (C) 2012 Bart Martens <bartm@knars.be>
# Copyright (C) 2015 Paul Wise <pabs@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

use strict;
use warnings;
use SOAP::Lite;
use POSIX;
use Dpkg::Deps qw(deps_parse deps_iterate);
use Dpkg::ErrorHandling qw(report_options);
use Dpkg::Control::Fields qw(field_list_src_dep field_list_pkg_dep field_get_dep_type);

# Silence dpkg warnings about deprecated relations
report_options(quiet_warnings => 1);

# Not interested in some of the fields
my %good_pkg = map { $_ => 1 } qw(Enhances Built-Using);
my %src_deps = map { $_ => 1 } grep { field_get_dep_type($_) ne 'union' } field_list_src_dep();
my %pkg_deps = map { $_ => 1 } grep { $good_pkg{$_} || field_get_dep_type($_) ne 'union' } field_list_pkg_dep();

# Work around SOAP::Lite not being able to verify certs correctly
my $ca_dir = '/etc/ssl/ca-debian';
$ENV{PERL_LWP_SSL_CA_PATH} = $ca_dir if -d $ca_dir;

my $soap = SOAP::Lite->uri('Debbugs/SOAP')->proxy('https://bugs.debian.org/cgi-bin/soap.cgi');
my @buglist = ();

foreach my $severity ( "grave", "serious", "critical" )
{
	foreach my $status ( "open", "forwarded" )
	{
		my $buglist = $soap->get_bugs(severity=>$severity,status=>$status)->result();
		push @buglist, @$buglist;
	}
}

my $depbuginfo = {};
while (my @slice = splice(@buglist, 0, 500))
{
	my $tmp = $soap->get_status(@slice)->result;
	next unless $tmp;
	%$depbuginfo = (%$depbuginfo, %$tmp);
}

my %monthabbr2number =
(
	"Jan" => 1,
	"Feb" => 2,
	"Mar" => 3,
	"Apr" => 4,
	"May" => 5,
	"Jun" => 6,
	"Jul" => 7,
	"Aug" => 8,
	"Sep" => 9,
	"Oct" => 10,
	"Nov" => 11,
	"Dec" => 12,
);

sub log_modified2time
{
	my $log_modified = shift;

	my $time = POSIX::ctime( $log_modified );
	$time = POSIX::mktime( 0, 0, 0, $2, $monthabbr2number{$1} - 1, $3 - 1900 )
		if( $time =~ /^\S+ (\S+) +(\d+) \d\d:\d\d:\d\d (\d{4})$/ );

	return $time;
}

sub log_modified2formatted_date
{
	my $log_modified = shift;
	my $date = log_modified2time( $log_modified );
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime( $date );
	my $formatted_date = strftime( "%F", $sec, $min, $hour, $mday, $mon, $year);

	return $formatted_date;
}

my $minimum_bug_age = 60*60*24*30;

foreach my $bug ( keys %$depbuginfo )
{
	next if( not defined $depbuginfo->{$bug} );

	my $youngest_bug = undef;
	my $most_recent_log_modified = 0;
	my $mergedwith = " ".$depbuginfo->{$bug}->{'mergedwith'}." $bug ";

	while( $mergedwith =~ s/ (\d+) / / )
	{
		next if( not defined $depbuginfo->{$1} );

		if( log_modified2time( $depbuginfo->{$1}->{"log_modified"} ) > $most_recent_log_modified )
		{
			$youngest_bug = $1;
			$most_recent_log_modified = log_modified2time( $depbuginfo->{$1}->{"log_modified"} );
		}
	}

	$mergedwith = " ".$depbuginfo->{$bug}->{'mergedwith'}." $bug ";

	while( $mergedwith =~ s/ (\d+) / / )
	{
		next if( not defined $depbuginfo->{$1} );

		delete $depbuginfo->{$1} if( $1 ne $youngest_bug );
	}
}

foreach my $bug ( keys %$depbuginfo )
{
	next if( ( " ".$depbuginfo->{$bug}->{"tags"}." " ) =~ / help / );
	next if( log_modified2time( $depbuginfo->{$bug}->{"log_modified"} ) < time - $minimum_bug_age );
	delete $depbuginfo->{$bug};
}

my %buggysource;

foreach my $bug ( keys %$depbuginfo )
{
	push @{$buggysource{$depbuginfo->{$bug}->{"source"}}}, $bug;
}

sub read_block
{
	my $block = "";

	while( <INPUT> )
	{
		$block .= $_;
		chomp;
		last if( /^$/ );
	}

	$block =~ s%\n(\s)%$1%gs;

	return undef if( $block eq "" );
	return $block;
}

sub simplify_packages_list
{
	my $type = shift;
	my $list = shift;
	if ($src_deps{$type} || $pkg_deps{$type}) {
		my $deps = deps_parse($list, build_dep => $src_deps{$type}, use_arch => 0, host_arch => 'amd64');
		my @deps; deps_iterate($deps, sub { push @deps, shift->{package}; });
		$list = join ',', @deps;
	} elsif ($type eq 'Source') {
		$list =~ s/ .*//;
	} elsif ($type eq 'Binary') {
		$list =~ tr/ //d;
	} else {
		warn "Unknown field: $type: $list";
	}
	$list = ",$list,";

	return $list;
}

# choices to make:
#my @mirrors = ( "debian", "debian-backports", "debian-security", "debian-volatile" );
my @mirrors = ( "debian", "debian-security" );
#my @dists = ( "oldstable", "stable", "testing", "unstable", "experimental" );
my @dists = ( "testing", "unstable", "experimental" );
#my @distvariants = ( "", "-proposed-updates", "-updates", "-backports" );
my @distvariants = ( "", "-proposed-updates", "-updates" );
my @sections = ( "main", "contrib", "non-free" );
my @sourcesfiles;
my @packagesfiles;

foreach my $mirror ( @mirrors )
{
	foreach my $dist ( @dists )
	{
		foreach my $distvariant ( @distvariants )
		{
			foreach my $section ( @sections )
			{
				push @sourcesfiles, "/srv/mirrors/$mirror/dists/$dist$distvariant/$section/source/Sources.gz";
				push @packagesfiles, "/srv/mirrors/$mirror/dists/$dist$distvariant/$section/binary-amd64/Packages.gz";
			}
		}
	}
}

my %bin2src;
my %bin2srcrdep;
my %src2srcrdep;
my %src2srcrdepall;

foreach my $sourcesfile ( @sourcesfiles )
{
	next if( ! -e $sourcesfile );
	open INPUT, "zcat $sourcesfile |" or next;
	while( $_ = read_block() )
	{
		my $source = undef;
		my $binary = undef;
		my %deps = ();

		$source = $1 if( /^Package: (.*)$/m );
		$binary = $1 if( /^Binary: (.*)$/m );
		foreach my $type (keys %src_deps) {
			$deps{$1} = $2 if( /^($type): (.*)/m );
		}

		die if( not defined $source );
		die if( not defined $binary );

		$binary = simplify_packages_list( 'Binary', $binary );
		while( $binary =~ s%,([^,]+),%,% )
		{
			$bin2src{$1}{$source} = 1;
		}

		foreach my $deptype ( keys %deps )
		{
			my $deps = simplify_packages_list( $deptype, $deps{$deptype} );
			while( $deps =~ s%,([^,]+),%,% )
			{
				$bin2srcrdep{$1}{$deptype}{$source} = 1;
			}
		}
	}
	close INPUT;
}

foreach my $packagesfile ( @packagesfiles )
{
	next if( ! -e $packagesfile );
	open INPUT, "zcat $packagesfile |" or next;
	while( $_ = read_block() )
	{
		my $binary = undef;
		my $source = undef;
		my %deps = ();

		$binary = $1 if( /^Package: (.*)/m );
		$source = $1 if( /^Source: (.*)/m );
		foreach my $type (keys %pkg_deps) {
			$deps{$1} = $2 if( /^($type): (.*)/m );
		}

		die if( not defined $binary );
		$source = $binary if( not defined $source );

		$source = simplify_packages_list( 'Source', $source );
		$source =~ s%^,([^,]+),$%$1%;

		$bin2src{$binary}{$source} = 1 if( not defined $bin2src{$binary}{$source} );

		foreach my $deptype ( keys %deps )
		{
			my $deps = simplify_packages_list( $deptype, $deps{$deptype} );
			while( $deps =~ s%,([^,]+),%,% )
			{
				$bin2srcrdep{$1}{$deptype}{$source} = 1;
			}
		}
	}
	close INPUT;
}

foreach my $binary ( keys %bin2srcrdep )
{
	next if( not defined $bin2src{$binary} );

	foreach my $deptype ( sort keys %{$bin2srcrdep{$binary}} )
	{
		foreach my $source ( keys %{$bin2srcrdep{$binary}{$deptype}} )
		{
			foreach my $source2 ( keys %{$bin2src{$binary}} )
			{
				next if( $source eq $source2 );
				next if( defined $src2srcrdep{$source2}{$source} );

				$src2srcrdep{$source2}{$source} = 1;
			}
		}
	}
}

my $added_packages = 0;
my $limit_rdepends = 150;

foreach my $source ( keys %buggysource )
{
	foreach my $rdep ( keys %{$src2srcrdep{$source}} )
	{
		$src2srcrdepall{$source}{$rdep} = 1;
		$added_packages = 1;
	}
}

while( 0 and $added_packages ) # disabled recursion on 20130510
{
	$added_packages = 0;

	foreach my $source ( keys %src2srcrdepall )
	{
		next if( scalar( keys %{$src2srcrdepall{$source}} ) > $limit_rdepends );

		foreach my $rdep ( keys %{$src2srcrdepall{$source}} )
		{
			next if( not defined $src2srcrdep{$rdep} );
			next if( scalar( keys %{$src2srcrdep{$rdep}} ) > $limit_rdepends );

			foreach my $rdep2 ( keys %{$src2srcrdep{$rdep}} )
			{
				next if( defined $src2srcrdepall{$source}{$rdep2} );

				$src2srcrdepall{$source}{$rdep2} = 1;
				$added_packages = 1;
			}
		}
	}
}

my %src2srcdepall;

foreach my $source ( keys %src2srcrdepall )
{
	foreach my $rdep ( keys %{$src2srcrdepall{$source}} )
	{
		$src2srcdepall{$rdep}{$source} = 1;
	}
}

foreach my $source ( keys %src2srcdepall )
{
	print "$source";

	foreach my $source2 ( keys %{$src2srcdepall{$source}} )
	{
		die if( not defined $buggysource{$source2} );

		foreach my $bug ( @{$buggysource{$source2}} )
		{
			print " $bug";
		}
	}

	print "\n";
}

