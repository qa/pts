
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
}
function setStyleSheet(theme) {
	var worked = false;
	var todisable = -1;
	var toenable = -1;
        for (i = 0; i < document.styleSheets.length; i++) {
		s = document.styleSheets[i];
		cssfile = s.href.substr(s.href.length - theme.length - 1, theme.length + 1);
		//alert(s.title + " " + cssfile + " " + (s.disabled ? "disabled" : "enabled"));
		if (s.title) {
			s.disabled = true;
			if (cssfile == "/" + theme) {
				s.disabled = false;
				worked = true;
			}
		}
        }
	return worked;
}
function onLoad() {
/*        var css = readCookie('csspref');
        var f = document.getElementById('csspref-form');
        var i = 0;
        var len = f.csspref.length;
	// Select the current style in the list
        for (i = 0; i < len; i++) {
                if (f.csspref.options[i].value == css) {
                        f.csspref.options[i].selected = true;
                }
        }*/
}
function onChangeStyle() {
        var f = document.getElementById('csspref-form');
	createCookie('csspref', f.csspref.value);
	if (!setStyleSheet(f.csspref.value)) {
		window.location.reload();
	}
}

function packageSubmit(form) {
	var package = form.src.value.trim();
	var len = 1;
	if (package.substring(0,3) == "lib") len = 4;
	location.href = "/" + package.substring(0,len) + "/" + package + ".html";
	return false;
}
