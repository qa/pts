<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"
  version="1.0">

  <xsl:template name="issue-nmu">
    <xsl:if test="@nmu">
      <li>Incorporate and acknowledge the changes from the non maintainer upload.</li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-piuparts">
    <xsl:if test="$hasother and $other/@piuparts='yes'">
      <li>
	<a href="https://piuparts.debian.org">Piuparts</a>
	<xsl:text> found an </xsl:text>
	<a href="https://piuparts.debian.org/sid/source/{$hash}/{$package}.html">(un)installation error</a>
	<xsl:text> in the </xsl:text>
	<a href="https://www.debian.org/releases/unstable/">Sid</a>
	<xsl:text> version of this package. You should investigate the cause; have a
	look at the </xsl:text>
	<a href="https://piuparts.debian.org/sid/source/{$hash}/{$package}.html">corresponding
	<em>piuparts log</em></a>
	<xsl:text>.</xsl:text>
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-watchbroken">
    <xsl:if test="$hasother and $other/@watchbroken='yes'">
      <li>
        <xsl:text>There is a </xsl:text>
        <a href="https://qa.debian.org/cgi-bin/watchfile-error.cgi?package={$escaped-package}">temporary or permanent problem</a>
        <xsl:text> with the debian/watch file included in the package.</xsl:text>
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-watchavail">
    <xsl:if test="$hasother and $other/@watchavail='yes'">
      <li>
        <xsl:text>Someone wrote </xsl:text>
        <a href="https://qa.debian.org/cgi-bin/watchfile.cgi?package={$escaped-package}">an updated debian/watch file</a>
        <xsl:text> you may want to include in the package.</xsl:text>
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-lintian">
    <xsl:if test="$lin_errs + $lin_warns > 0">
      <li>
	<xsl:variable name="lintian_url"><xsl:call-template name="mk_lintian_url" /></xsl:variable>
	<a href="https://udd.debian.org/lintian/">Lintian</a><xsl:text> </xsl:text>
	<xsl:text>reports </xsl:text>
	<a href="{$lintian_url}">
	  <xsl:if test="$lin_errs > 0"><xsl:value-of select="$lin_errs" /> error<xsl:if test="$lin_errs > 1">s</xsl:if></xsl:if>
	  <xsl:if test="$lin_errs > 0 and $lin_warns > 0"> and </xsl:if>
	  <xsl:if test="$lin_warns > 0"><xsl:value-of select="$lin_warns" /> warning<xsl:if test="$lin_warns > 1">s</xsl:if></xsl:if>
	</a>
	about this package. You should make the package <em>lintian clean</em>
	getting rid of <xsl:if test="$lin_errs + $lin_warns = 1">it</xsl:if>
	<xsl:if test="$lin_errs + $lin_warns > 1">them</xsl:if>.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-logcheck">
    <xsl:if test="$logcheck_errs + $logcheck_warns > 0">
      <li>
	<xsl:variable name="logcheck_url"><xsl:call-template name="mk_logcheck_url" /></xsl:variable>
	<a href="https://qa.debian.org/bls/index.html">Build log checks</a><xsl:text> </xsl:text>
	<xsl:text>report </xsl:text>
	<a href="{$logcheck_url}">
	  <xsl:if test="$logcheck_errs > 0"><xsl:value-of select="$logcheck_errs" /> error<xsl:if test="$logcheck_errs > 1">s</xsl:if></xsl:if>
	  <xsl:if test="$logcheck_errs > 0 and $logcheck_warns > 0"> and </xsl:if>
	  <xsl:if test="$logcheck_warns > 0"><xsl:value-of select="$logcheck_warns" /> warning<xsl:if test="$logcheck_warns > 1">s</xsl:if></xsl:if>
	</a>
	about this package.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-mentors-pending">
    <xsl:if test="$hasmentors"> <!-- todo item about sponsoring mentors.d.n.  uploads -->
      <xsl:variable name="mentors_version"
	select="document(concat('../base/', $dir, '/mentors.xml'))/source/version" />
      <li>
	<a href="https://mentors.debian.net"><tt>mentors.debian.net</tt></a> has
	<a href="https://mentors.debian.net/package/{$escaped-package}">version
	  <xsl:value-of select="$mentors_version" /></a> of this package, you
	should consider sponsoring its upload.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-comaintenance">
    <xsl:if test="not(uploaders)and(priority='standard' or priority='required' or priority='important')">
      <li>The package is of priority standard or higher, you should really find some co-maintainers.</li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-outdate-stdver">
    <xsl:if test="standards-version!='' and not(starts-with(standards-version, $lastsv))">
      <xsl:variable name="lastsv-uc" select="str:replace($lastsv, '.', '-')"/>
      <xsl:variable name="sv-uc" select="str:replace(standards-version, '.', '-')"/>
      <li>The package should be updated to follow the last version of
	<a href="https://www.debian.org/doc/debian-policy/">Debian Policy</a> (Standards-Version 
	<a href="https://www.debian.org/doc/debian-policy/upgrading-checklist.html#version-{$lastsv-uc}"><xsl:value-of select="$lastsv"/></a> instead of 
	<a href="https://www.debian.org/doc/debian-policy/upgrading-checklist.html#version-{$sv-uc}"><xsl:value-of select="standards-version"/></a>).
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-new-upstream">
    <xsl:if test="$hasother and $other[dehs/@newer] and not($other/dehs/@newer=$other/@new_version_upstream)">
      <li>A new upstream version is available:
	<a href="{$other/dehs/@url}">
	  <xsl:value-of select="$other/dehs/@newer"/>
      </a>, you should consider packaging it.</li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-patches">
    <xsl:if test="$hasother and $other/bugs/@patch!='0'">
      <li>The <acronym title="Bug Tracking System">BTS</acronym> contains
	<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?src={$escaped-package}&amp;include=tags:patch&amp;exclude=tags:pending&amp;pend-exc=done&amp;repeatmerged=no">patches fixing
	  <xsl:value-of select="$other/bugs/@patch"/> bug<xsl:if test="$other/bugs/@patch!='1'">s</xsl:if>
	</a>
	<xsl:if test="$other/bugs/@patch_m">
	  <xsl:text> (</xsl:text>
	  <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?src={$escaped-package}&amp;include=tags:patch&amp;exclude=tags:pending&amp;pend-exc=done&amp;repeatmerged=yes">
	    <xsl:value-of select="$other/bugs/@patch_m"/>
	  </a>
	  <xsl:text> if counting merged bugs)</xsl:text>
	</xsl:if>, consider including or untagging
	<xsl:if test="$other/bugs/@patch!='1'">them</xsl:if>
	<xsl:if test="$other/bugs/@patch='1'">it</xsl:if>.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-testing-excuses">
    <xsl:param name="section" />
    <xsl:if test="$hasexcuse and document(concat('../base/', $dir, '/excuse.xml'))/excuse/@problematic">
      <li>The package has not yet entered <a
	  href="https://release.debian.org/britney/update_excuses.html#{$package}">testing</a>
	even though the <xsl:value-of select="document(concat('../base/', $dir, '/excuse.xml'))/excuse/@limit"/>-day
	delay is over.
	<a href="https://qa.debian.org/excuses.php?package={$escaped-package}">Check why</a>.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-ancient-stdver">
    <xsl:if test="standards-version!='' and not(starts-with(standards-version, $lastmajorsv))">
      <li>The package is severely out of date with respect to the Debian
	Policy. Latest version is <xsl:value-of select="$lastsv"/>
	and your package only follows
	<xsl:value-of select="standards-version"/>...</li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-dead-package">
    <xsl:if test="@release!='unstable' and @release!='experimental'">
      <li>This package is neither part of unstable nor experimental. This
	probably means that the package <a
	  href="https://ftp-master.debian.org/removals.txt">has been removed</a> (or
	has been renamed). Thus the information here is of little interest ...
	the package is going to disappear unless someone takes it over and
	reintroduces it into unstable.</li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-override-disparity">
    <xsl:if test="$hasother and $other/@override='yes'"> <!-- Override disparity handling. [JvW] -->
      <xsl:for-each select="$other/override/group">
	<li>There were override disparities found in suite <xsl:value-of
	    select="@suite"/>:
	  <ul>
	    <xsl:for-each select="disparity">
	      <li><xsl:value-of select="text()"/></li>
	    </xsl:for-each>
	  </ul>
	</li>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-help-bugs"> <!-- mention bugs tagged "help" -->
    <xsl:if test="$hasother and $other/bugs/@help > 0">
      <li>
	The <acronym title="Bug Tracking System">BTS</acronym> contains
	<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=help&amp;src={$package}&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done">
	  <span class="bugcount" title="help">
	    <xsl:value-of select="$other/bugs/@help" />
	  </span>
	  bug<xsl:if test="$other/bugs/@help > 1">s</xsl:if>
	</a>
	tagged <a href="https://www.debian.org/Bugs/Developer#tags"><tt>help</tt></a>,
	please consider helping the maintainer in dealing with
	<xsl:choose>
	  <xsl:when test="$other/bugs/@help = 1">it.</xsl:when>
	  <xsl:when test="$other/bugs/@help > 1">them.</xsl:when>
	</xsl:choose>
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-wnpp"> <!-- Wnpp handling. [PvR] -->
    <xsl:if test="$hasother and $other/@wnpp='yes'">
      <li>
	<xsl:choose>
	  <xsl:when test="$other/wnpp/@type='O'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an O (Orphaned) entry for this
		package. This is probably an error, as it is neither part of
		unstable nor experimental.
	      </xsl:when>
	      <xsl:otherwise>
		<span style="font-weight: bold">This package has been
		  orphaned</span>.  This means that it does not have a real
		maintainer at the moment. Please consider adopting this package
		if you are interested in it.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='ITA'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an ITA (Intent To Adopt) entry for
		this package. This is probably an error, as it is neither part
		of unstable nor experimental.
	      </xsl:when>
	      <xsl:otherwise>
		This package has been orphaned, but someone intends to maintain
		it.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='RFA'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an RFA (Request For Adoption) entry
		for this package. This is probably an error, as it is neither
		part of unstable nor experimental.
	      </xsl:when>
	      <xsl:otherwise>
		The current maintainer is looking for someone who can take over
		maintenance of this package. If you are interested in this
		package, please consider taking it over. Alternatively you may
		want to be co-maintainer in order to help the actual
		maintainer.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='RFH'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an RFH (Request For Help) entry for
		this package. This is probably an error, as it is neither part
		of unstable nor experimental.
	      </xsl:when>
	      <xsl:otherwise>
		The current maintainer is looking for someone who can help with
		the maintenance of this package. If you are interested in this
		package, please consider helping out. One way you can help is
		offer to be a co-maintainer or triage bugs in the bts.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='ITP'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an ITP (Intent To Package). This
		probably means that somebody is going to reintroduce this
		package into unstable.
	      </xsl:when>
	      <xsl:otherwise>
		The WNPP database contains an ITP (Intent To Package) entry for
		this package. This is probably an error, as it has already been
		packaged.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='RFP'">
	    <xsl:choose>
	      <xsl:when test="@release!='unstable' and @release!='experimental'">
		The WNPP database contains an RFP (Request For Package). This
		probably means that somebody would like to see this package
		reintroduced into unstable by a volunteer.
	      </xsl:when>
	      <xsl:otherwise>
		The WNPP database contains an RFP (Request For Package) entry
		for this package.  This is probably an error, as it has already
		been packaged.
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:when>
	  <xsl:when test="$other/wnpp/@type='RM'">
	    <span style="font-weight: bold">This package has been requested to
	      be removed</span>.  This means that, when this request gets
	    processed by an ftp-master, this package will no longer be in
	    unstable, and will automatically be removed from testing too
	    afterwards. If for some reason you want keep this package in
	    unstable, please discuss so in the bug.
	  </xsl:when>
	  <xsl:otherwise>
	    The WNPP database contains an entry for this package, but it is
	    unclear what kind of entry it is. This is probably an error.
	  </xsl:otherwise>
	</xsl:choose>
	<xsl:variable name="bn" select="$other/wnpp/@bugnumber"/>
	Please see bug number <a href="https://bugs.debian.org/{$bn}">
	  #<xsl:value-of select="$bn"/></a> for more information.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-watch-failure">
    <xsl:if test="$hasother and $other/@watch='yes'">
      <li>
	<xsl:if test="$other/watch/@warning!=''">
	  uscan had problems while searching for a new upstream version: <xsl:value-of select="$other/watch/@warning"/>
	</xsl:if>
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-item-dehs-failure">
    <xsl:if test="$hasother and $other[dehs/@error]">
      <li>The package has a <kbd>debian/watch</kbd> file, but the last attempt
	to use it for checking for newer upstream versions failed with
	an error: <pre style="overflow: auto"><xsl:value-of select="$other/dehs/@error"/></pre></li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-l10n">
    <xsl:if test="$hasother and $other/i18n/@todo='yes'
		  and $other/i18n/@href">
      <li>
	Some strings of this package need translation. You should
	check the <a href="{$other/i18n/@href}">l10n status report</a>
	for more information.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-security">
    <xsl:if test="$hasother and $other/@security!='0'">
      <li>
        There
        <xsl:choose>
          <xsl:when test="$other/@security!='1'">are</xsl:when>
	  <xsl:when test="$other/@security='1'">is</xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
	<a href="https://security-tracker.debian.org/tracker/source-package/{$escaped-package}">
	  <xsl:value-of select="$other/@security"/>
	  open security issue<xsl:if test="$other/@security!='1'">s</xsl:if>
        </a>, please fix 
        <xsl:choose>
	  <xsl:when test="$other/@security!='1'">them</xsl:when>
	  <xsl:when test="$other/@security='1'">it</xsl:when>
	</xsl:choose>.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-release-goals">
    <xsl:if test="$hasother and $other/@releasegoals='yes'">
      <xsl:for-each select="$other/releasegoals/bug">
        <li>
	  Bug <a href="https://bugs.debian.org/{@id}">#<xsl:value-of select="@id" /></a>
	  must be fixed to complete Debian's release goal known as
	  “<a href="{@url}"><xsl:value-of select="@name" /></a>”.
        </li>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-depneedsmaint">
    <xsl:if test="$hasother and $other/@depneedsmaint='yes'">
      <xsl:for-each select="$other/depneedsmaint/package">
        <li>
          This package has "<xsl:value-of select="@deptype" />: <xsl:value-of select="@binpkg" />"
          while <a href="https://packages.qa.debian.org/{@name}"><xsl:value-of select="@name" /></a>
          needs a new maintainer, see
          <a href="https://bugs.debian.org/{@wnppbug}"><xsl:value-of select="@wnpptype" /> #<xsl:value-of select="@wnppbug" /></a>.
        </li>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-urls">
    <xsl:param name="email" />
    <xsl:if test="$hasother and $other/@urlissues='yes'">
      <li>
        The URL(s) for this package had some recent persistent
        <a href="http://duck.debian.net/static/sp/{$hash}/{$package}.html">issues</a>.
      </li>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-dedup">
    <xsl:if test="$hasother and $other/@dedup='yes'">
      <xsl:for-each select="$other/dedup/item">
        <li>
          Package <xsl:value-of select="@pkg_a" />
          <xsl:text> </xsl:text>
          <a href="https://dedup.debian.net/compare/{@pkg_a}/{@pkg_b}">shares
          <xsl:value-of select="@size" /> of similar files</a> with
          <xsl:choose>
            <xsl:when test="@pkg_a=@pkg_b">itself,</xsl:when>
            <xsl:otherwise>package <xsl:value-of select="@pkg_b" />,</xsl:otherwise>
          </xsl:choose>
          please investigate whether it is possible to
          <a href="https://wiki.debian.org/dedup.debian.net">reduce the duplication</a>.
        </li>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template name="issue-testing-failed">
    <xsl:if test="$hasother and $other/@testing='yes' and $other/testing/@status!='pass'">
      <li>
        The as-installed testing for this package
        <a title="as-installed package tests {$other/testing/@status}" href="https://ci.debian.net/packages/{$hash}/{$escaped-package}/">did not pass</a>
        (<a title="as-installed package tests log file"
         href="https://ci.debian.net/data/packages/unstable/amd64/{$hash}/{$escaped-package}/latest-autopkgtest/log.gz">log</a>).
      </li>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
