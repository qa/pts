<?xml version="1.0" encoding="utf-8" ?>
<!--
  Copyright (C) 2007-2008 Stefano Zacchiroli <zack@debian.org>

  This file is distributed under the terms of the General Public License
  version 3 or (at your option) any later version.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
>

  <xsl:param name="package"/> <!-- e.g. "vim" -->
  <xsl:param name="hash"/>    <!-- e.g. "v", can be like "libv" for libs -->

  <xsl:variable name="ptsurl">
    <xsl:text>https://packages.qa.debian.org</xsl:text>
  </xsl:variable>

  <xsl:variable name="baseurl">
    <!-- e.g. "https://packages.qa.debian.org/v/vim" -->
    <!-- no trailing slash! -->
    <xsl:value-of select="$ptsurl" />
    <xsl:text>/</xsl:text> <xsl:value-of select="$hash" />
    <xsl:text>/</xsl:text> <xsl:value-of select="$package" />
  </xsl:variable>

  <xsl:variable name="pkgurl">
    <!-- e.g. "https://packages.qa.debian.org/v/vim.html" -->
    <xsl:value-of select="$baseurl" />
    <xsl:text>.html</xsl:text>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates select="news/news" />
  </xsl:template>

  <xsl:template match="news">
    <rss version="2.0">
      <channel>
	<title>
	  <xsl:text>Debian developer's news for </xsl:text>
	  <xsl:value-of select="$package" />
	</title>
	<link>
	  <xsl:value-of select="$pkgurl" />
	</link>
	<description>
	  <xsl:text>Latest developer's news for Debian source package </xsl:text>
	  <xsl:value-of select="$package" />.
	</description>
	<language>en-us</language>
	<xsl:apply-templates />
      </channel>
    </rss>
  </xsl:template>

  <xsl:template match="item">
    <item>
      <title>
	<xsl:value-of select="." />
	<xsl:text> (</xsl:text>
	<xsl:value-of select="@from" />
	<xsl:text>)</xsl:text>
      </title>
      <xsl:variable name="id">
	<xsl:value-of select="$ptsurl" />
	<xsl:text>/</xsl:text> <xsl:value-of select="$hash" />
	<xsl:text>/</xsl:text> <xsl:value-of select="@url" />
      </xsl:variable>
      <pubDate> <xsl:value-of select="@rfc822date" /> </pubDate>
      <guid> <xsl:value-of select="$id" /> </guid>
      <link> <xsl:value-of select="$id" /> </link>
      <description>
        <xsl:text>&lt;a href="https://packages.qa.debian.org/</xsl:text>
        <xsl:value-of select="$hash" />
        <xsl:text>/</xsl:text>
        <xsl:value-of select="@url" />
        <xsl:text>"&gt;[</xsl:text>
        <xsl:value-of select="@date" />
        <xsl:text>] </xsl:text>
        <xsl:value-of select="." />
        <xsl:text> (</xsl:text>
        <xsl:value-of select="@from" />
        <xsl:text>)&lt;/a&gt;</xsl:text>
      </description>
    </item>
  </xsl:template>

</xsl:stylesheet>
