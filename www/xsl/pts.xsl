<?xml version="1.0" encoding="utf-8" ?>

<!--
# Copyright © 2002-2011 Raphaël Hertzog and others
# Copyright © 2007-2009 Stefano Zacchiroli
# This file is distributed under the terms of the General Public License
# version 2 or (at your option) any later version.
-->

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"
  version="1.0">

<xsl:output
  method="xml"
  encoding="UTF-8"
  omit-xml-declaration="yes"
  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
  indent="yes" />

<xsl:include href="pts-issues.xsl" />

<!-- Include common parameters and variables definitions to pts.xsl and admssw.xsl -->
<xsl:include href="common-params-vars.xsl" />


<xsl:variable name="low-nmu-emails"
  select="document('../base/low_threshold_nmu.emails.xml')/emails"/>

<!-- Those variables controls the todo and problem item concerning
     standards-version not being up to date -->
<xsl:variable name="lastsv" select="document('../base/d/debian-policy/unstable.xml')/source/standards-version"/>
<xsl:variable name="lastmajorsv" select="concat(str:split($lastsv, '.')[1], '.')"/>

<!-- lintian summary -->
<xsl:variable name="lin_errs" select="$other/lintian/@errors" />
<xsl:variable name="lin_warns" select="$other/lintian/@warnings" />

<!-- buildd log check summary -->
<xsl:variable name="logcheck_errs" select="$other/logcheck/@errors" />
<xsl:variable name="logcheck_warns" select="$other/logcheck/@warnings" />

<!-- Named templates aka functions -->

<xsl:template name="mk_lintian_url">
  <xsl:text>https://udd.debian.org/lintian/?packages=</xsl:text>
  <xsl:value-of select="$escaped-package" />
</xsl:template>

<xsl:template name="mk_logcheck_url">
  <xsl:text>https://qa.debian.org/bls/packages/</xsl:text>
  <xsl:value-of select="substring($package,1,1)"/>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="$package" />
  <xsl:text>.html</xsl:text>
</xsl:template>

<xsl:template name="mk_upstream_url">
  <xsl:text>https://anonscm.debian.org/viewvc/collab-qa/packages-metadata/</xsl:text>
  <xsl:value-of select="substring($package,1,1)"/>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="$package" />
  <xsl:text>.upstream?view=co</xsl:text>
</xsl:template>

<xsl:variable name="all-archs-mirror">http://deb.debian.org/debian</xsl:variable>
<xsl:variable name="security-mirror">http://security.debian.org/</xsl:variable>

<xsl:template name="outputitem">
  <xsl:choose>
    <xsl:when test="@url">
      <li><xsl:if test="@date">
        <xsl:text>[</xsl:text>
	<xsl:value-of select="@date"/>
        <xsl:text>] </xsl:text>
      </xsl:if><a href="{@url}">
      <xsl:value-of select="text()"/></a><xsl:if test="@from">
        <xsl:text> (</xsl:text>
	<xsl:value-of select="@from"/>
        <xsl:text>)</xsl:text></xsl:if></li>
    </xsl:when>
    <xsl:when test="@type='raw'">
      <li>
	<xsl:copy-of select="node()" />
      </li>
    </xsl:when>
    <xsl:otherwise>
      <li><xsl:if test="@date">
        <xsl:text>[</xsl:text>
	<xsl:value-of select="@date"/>
        <xsl:text>] </xsl:text>
      </xsl:if><xsl:value-of select="text()"/></li>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Convert + in %2b for URL escaping. Should actually first also do
other-to-%xx, especially % to %25... Fortunately, that's rare -->
<xsl:template name="escape-name">
  <xsl:param name="text"/>
  <xsl:if test="contains($text,'+')">
    <xsl:value-of select="substring-before($text,'+')"/>
    <xsl:text>%2b</xsl:text>
    <xsl:call-template name="escape-name">
    <xsl:with-param name="text"><xsl:value-of select="substring-after($text,'+')"/></xsl:with-param>
    </xsl:call-template>
  </xsl:if>
  <xsl:if test="not(contains($text,'+'))">
    <xsl:value-of select="$text"/>
  </xsl:if>
</xsl:template>

<xsl:variable name="escaped-package">
  <xsl:call-template name="escape-name">
    <xsl:with-param name="text"><xsl:value-of select="$package"/></xsl:with-param>
  </xsl:call-template>
</xsl:variable>

<!-- Strip epoch -->
<xsl:template name="strip-epoch">
  <xsl:param name="version"/>
  <xsl:if test="contains($version,':')">
    <xsl:value-of select="substring-after($version,':')"/>
  </xsl:if>
  <xsl:if test="not(contains($version,':'))">
    <xsl:value-of select="$version"/>
  </xsl:if>
</xsl:template>

<xsl:template name="add-vcs-info">
  <xsl:if test="repository">
    <dt title="Version Control System">VCS</dt>
    <dd>
      <xsl:if test="repository/vcs[@kind!='browser']">
        <xsl:for-each select="repository/vcs[@kind!='browser']">
          <xsl:sort select="@kind" />
          <a title="raw {@kind} repository" href="{@url}">
            <xsl:value-of select="@kind" />
          </a>
          <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
        </xsl:for-each>
      </xsl:if>
      <xsl:if test="repository/vcs[@kind='browser']">
        <xsl:text> (</xsl:text>
        <a title="browse the repository"
           href="{repository/vcs[@kind='browser']/@url}">browse</a>
          <xsl:text>)</xsl:text>
      </xsl:if>
    </dd>
  </xsl:if>
</xsl:template>

<xsl:template name="lists-archive-info">
  <xsl:param name="name" />
  <xsl:param name="email" />
  <xsl:variable name="user">
    <xsl:value-of select="substring-before($email,'@')"/>
  </xsl:variable>
  <xsl:variable name="domain">
    <xsl:value-of select="substring-after($email,'@')"/>
  </xsl:variable>
  <xsl:variable name="archives">
    <xsl:choose>
      <xsl:when test="$domain='lists.debian.org'">
        <xsl:value-of select="concat('https://',$domain,'/',$user,'/')"/>
      </xsl:when>
      <xsl:when test="$domain='lists.alioth.debian.org'">
        <xsl:value-of select="concat('https://',$domain,'/pipermail/',$user,'/')"/>
      </xsl:when>
      <xsl:when test="$domain='lists.ubuntu.com'">
        <xsl:value-of select="concat('https://',$domain,'/archives/',$user,'/')"/>
      </xsl:when>
      <xsl:when test="$domain='lists.riseup.net'">
        <xsl:value-of select="concat('https://',$domain,'/www/arc/',$user)"/>
      </xsl:when>
      <xsl:when test="$domain='lists.launchpad.net'">
        <xsl:value-of select="concat('https://',$domain,'/',$user,'/')"/>
      </xsl:when>
      <xsl:when test="$domain='lists.freedesktop.org'">
        <xsl:value-of select="concat('http://',$domain,'/archives/',$user,'/')"/>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:if test="$archives!=''">
    <small>
      <xsl:text> (</xsl:text>
        <a href="{$archives}" title="Archives for the {$name} list">a</a>
      <xsl:text>)</xsl:text>
    </small>
  </xsl:if>
</xsl:template>

<xsl:template name="add-maintenance-info">
  <xsl:param name="email" />
  <xsl:variable name="lownmu">
    <xsl:if test="$low-nmu-emails/email[text()=$email]">
      <xsl:text>true</xsl:text>
    </xsl:if>
  </xsl:variable>

  <xsl:if test="string($lownmu)!=''">
    <div class="maint-markers">
    <xsl:if test="string($lownmu)!=''">
      <span class="lownmu-tag">
	<a href="https://wiki.debian.org/LowThresholdNmu">
	  <acronym title="maintainer agrees with Low Threshold NMU">LowNMU</acronym>
	</a>
      </span>
    </xsl:if>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="output-news">
  <xsl:param name="news" />

  <xsl:if test="count($news)>0 and string($news)!=''">
    <div class="block news">
      <a name="news" />
      <h2>news <a class="feedlink" href="{$package}/news.rss20.xml">RSS</a></h2>
      <ul id="news-list">
	<xsl:copy-of select="$news" />
      </ul>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="output-static">
  <xsl:param name="static" />

  <xsl:if test="count($static)>0 and string($static)!=''">
    <div class="block static">
      <a name="static" />
      <h2>static info</h2>
      <ul>
	<xsl:copy-of select="$static" />
      </ul>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="maintainer-email">
  <xsl:param name="email" />
  <a class="email" href="mailto:{$email}">
    <img alt="[email]" src="../common/email.png" title="email" />
  </a>
</xsl:template>

<xsl:template name="general-information">
  <div class="block info">
    <a name="general" />
    <h2>general</h2>
    <dl>
      <dt>source</dt>
      <dd>
	<a href="https://packages.debian.org/src:{$package}">
	  <xsl:value-of select="$package" />
	</a>
	(<span id="priority" title="priority">
	  <small><xsl:value-of select="priority"/></small>
	</span>,
	<span id="section" title="section">
	  <small><xsl:value-of select="section"/></small>
	</span>)
      </dd>

      <dt>version</dt>
      <dd>
	<span id="latest_version"><xsl:value-of select="version"/></span>
      </dd>

      <xsl:if test="@release!='unstable'">
	<dt>distro</dt>
	<dd><xsl:value-of select="@release"/></dd>
      </xsl:if>

      <dt title="Maintainer and Uploaders">maint</dt>
      <dd class="maintainer">
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://qa.debian.org/developer.php?login=</xsl:text>
	    <xsl:call-template name="escape-name">
	      <xsl:with-param name="text"><xsl:value-of select="maintainer/email"/></xsl:with-param>
	    </xsl:call-template>
	  </xsl:attribute>
	  <span class="name" title="maintainer">
	    <xsl:value-of select="maintainer/name"/>
	  </span>
	</xsl:element>
        <xsl:call-template name="lists-archive-info">
          <xsl:with-param name="name"><xsl:value-of select="maintainer/name"/></xsl:with-param>
          <xsl:with-param name="email"><xsl:value-of select="maintainer/email"/></xsl:with-param>
        </xsl:call-template>
	<xsl:if test="$hasother and $other/dms/item/@email=maintainer/email">
          <xsl:text> </xsl:text>
          <small>(dm)</small>
	</xsl:if>
	<xsl:if test="uploaders">
          <xsl:for-each select="uploaders/item">
	    <xsl:text>, </xsl:text>
	    <span class="uploader">
	      <small>
	      <xsl:element name="a">
		<xsl:attribute name="href">
		  <xsl:text>https://qa.debian.org/developer.php?login=</xsl:text>
		  <xsl:call-template name="escape-name">
		    <xsl:with-param name="text">
		      <xsl:value-of select="email"/>
		    </xsl:with-param>
		  </xsl:call-template>
		</xsl:attribute>
		<span class="name" title="uploader">
		  <xsl:value-of select="name"/>
		</span>
	      </xsl:element>
              <xsl:call-template name="lists-archive-info">
                <xsl:with-param name="name"><xsl:value-of select="name"/></xsl:with-param>
                <xsl:with-param name="email"><xsl:value-of select="email"/></xsl:with-param>
              </xsl:call-template>
	      <xsl:text> (u</xsl:text>
	      <xsl:if test="$hasother and $other/dms/item/@email=email">
		<xsl:text>, dm</xsl:text>
	      </xsl:if>
	      <xsl:text>)</xsl:text>
	      </small>
	    </span>
          </xsl:for-each>
	</xsl:if>
	<xsl:call-template name='add-maintenance-info'>
	  <xsl:with-param name="email" select="maintainer/email" />
	</xsl:call-template>
      </dd>

      <xsl:if test="architecture!='any' and architecture!='all'">
	<dt>arch</dt>
	<dd><xsl:value-of select="architecture"/></dd>
      </xsl:if>

      <dt title="Standards-Version">std-ver</dt>
      <dd>
	<span id="standards_version">
	  <xsl:choose>
	    <xsl:when test="string-length(standards-version) = 0">
	      <xsl:text>n/a</xsl:text>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="standards-version"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</span>
      </dd>

      <xsl:call-template name='add-vcs-info' />
    </dl>
  </div>
</xsl:template>

<xsl:template name="bugs-count">
  <div class="block bugs">
    <a name="bugs" />
    <h2>bugs</h2>
    <dl>
      <dt id="bugs_all">
	all
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://qa.debian.org/data/bts/graphs/</xsl:text>
	    <xsl:value-of select="$dir"/>
	    <xsl:text>.png</xsl:text>
	  </xsl:attribute>
	  <xsl:attribute name="title">bug history graph</xsl:attribute>
	  <img alt="bug history graph" src="../common/bug-graph.png" />
	</xsl:element>
      </dt>
      <dd>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?repeatmerged=no&amp;src=</xsl:text>
	    <xsl:value-of select="$escaped-package" />
	  </xsl:attribute>
	  <xsl:if test="$hasother">
	    <span class="bugcount" title="all">
	      <xsl:value-of select="$other/bugs/@all"/>
	    </span>
	  </xsl:if>
	</xsl:element>
	<xsl:if test="$hasother and $other/bugs/@all_m">
	  (<xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?repeatmerged=yes&amp;src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	    </xsl:attribute>
	    <xsl:value-of select="$other/bugs/@all_m"/>
	  </xsl:element>)
	</xsl:if>
      </dd>
      <dt id="bugs_rc" title="Release Critical">
	<span class="indented">RC</span>
      </dt>
      <dd>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	    <xsl:value-of select="$escaped-package" />
	    <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious&amp;repeatmerged=no</xsl:text>
	  </xsl:attribute>
	  <xsl:if test="$hasother">
	    <span class="bugcount" title="rc">
	      <xsl:value-of select="$other/bugs/@rc"/>
	    </span>
	  </xsl:if>
	</xsl:element>
	<xsl:if test="$hasother and $other/bugs/@rc_m">
	  (<xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	      <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious&amp;repeatmerged=yes</xsl:text>
	    </xsl:attribute>
	    <xsl:value-of select="$other/bugs/@rc_m"/>
	  </xsl:element>)
	</xsl:if>
      </dd>
      <dt id="bugs_in" title="Important and Normal">
	<span class="indented">I&amp;N</span>
      </dt>
      <dd>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	    <xsl:value-of select="$escaped-package" />
	    <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=important&amp;sev-inc=normal&amp;repeatmerged=no</xsl:text>
	  </xsl:attribute>
	  <xsl:if test="$hasother">
	    <span class="bugcount" title="in">
	      <xsl:value-of select="$other/bugs/@normal"/>
	    </span>
	  </xsl:if>
	</xsl:element>
	<xsl:if test="$hasother and $other/bugs/@normal_m">
	  (<xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	      <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=important&amp;sev-inc=normal&amp;repeatmerged=yes</xsl:text>
	    </xsl:attribute>
	    <xsl:value-of select="$other/bugs/@normal_m"/>
	  </xsl:element>)
	</xsl:if>
      </dd>
      <dt id="bugs_mw" title="Minor and Wishlist">
	<span class="indented">M&amp;W</span>
      </dt>
      <dd>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	    <xsl:value-of select="$escaped-package" />
	    <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=minor&amp;sev-inc=wishlist&amp;repeatmerged=no</xsl:text>
	  </xsl:attribute>
	  <xsl:if test="$hasother">
	    <span class="bugcount" title="mw">
	      <xsl:value-of select="$other/bugs/@wishlist"/>
	    </span>
	  </xsl:if>
	</xsl:element>
	<xsl:if test="$hasother and $other/bugs/@wishlist_m">
	  (<xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	      <xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=minor&amp;sev-inc=wishlist&amp;repeatmerged=yes</xsl:text>
	    </xsl:attribute>
	    <xsl:value-of select="$other/bugs/@wishlist_m"/>
	  </xsl:element>)
	</xsl:if>
      </dd>
      <dt id="bugs_fp" title="Fixed and Pending">
	<span class="indented">F&amp;P</span>
      </dt>
      <dd>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	    <xsl:value-of select="$escaped-package" />
	    <xsl:text>&amp;archive=no&amp;pend-inc=pending-fixed&amp;pend-inc=fixed&amp;repeatmerged=no</xsl:text>
	  </xsl:attribute>
	  <xsl:if test="$hasother">
	    <span class="bugcount" title="fp">
	      <xsl:value-of select="$other/bugs/@fixed"/>
	    </span>
	  </xsl:if>
	</xsl:element>
	<xsl:if test="$hasother and $other/bugs/@fixed_m">
	  (<xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	      <xsl:text>&amp;archive=no&amp;pend-inc=pending-fixed&amp;pend-inc=fixed&amp;repeatmerged=yes</xsl:text>
	    </xsl:attribute>
	    <xsl:value-of select="$other/bugs/@fixed_m"/>
	  </xsl:element>)
	</xsl:if>
      </dd>
      <xsl:if test="$other/bugs/@newcomer &gt; 0">
	<dt id="bugs_newcomer" title="Newcomer">
	  <span class="indented">
	    <a href="https://wiki.debian.org/BTS/NewcomerTag">NC</a>
	  </span>
	</dt>
	<dd>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	    <xsl:text>;tag=newcomer</xsl:text>
	    </xsl:attribute>
	    <span class="bugcount" title="newcomer">
	      <xsl:value-of select="$other/bugs/@newcomer" />
	    </span>
	  </xsl:element>
	</dd>
      </xsl:if>
      <xsl:if test="$hasother and $other/bugs/@help &gt; 0">
	<dt id="bugs_help" title="Help needed">
	  <span class="indented">Help</span>
	</dt>
	<dd>
	  <xsl:element name="a">
	    <xsl:attribute name="href">
	      <xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=</xsl:text>
	      <xsl:value-of select="$escaped-package" />
	    <xsl:text>;tag=help</xsl:text>
	    </xsl:attribute>
	    <span class="bugcount" title="help">
	      <xsl:value-of select="$other/bugs/@help" />
	    </span>
	  </xsl:element>
	</dd>
      </xsl:if>
      <xsl:if test="$other/@ubuntu='yes'">
        <xsl:if test="$other/ubuntu/bugs">
          <dt>
            <xsl:call-template name="ubuntu">
              <xsl:with-param name="text">Ubu</xsl:with-param>
            </xsl:call-template>
          </dt>
          <dd>
	    <a href="{$other/ubuntu/bugs/@url}">
              <xsl:value-of select="$other/ubuntu/bugs/@count"/>
            </a>
          </dd>
        </xsl:if>
      </xsl:if>
    </dl>
  </div>
</xsl:template>

<xsl:template name="pts-subscription">
  <!-- <acronym title="Package Tracking System">PTS</acronym> subscription -->
  <!-- <tr class="normalrow"> -->
  <!--   <td class="labelcell">Subscribers count</td> -->
  <!--   <td class="contentcell"><xsl:if test="$hasother"> -->
  <!-- 	<xsl:value-of select="$other/pts/@count"/> -->
  <!--   </xsl:if></td> -->
  <!-- </tr> -->
  <form method="post" action="/cgi-bin/pts.cgi">
    <p>
      <input type="hidden" name="package" value="{$package}"/>
      subscribe to this package
      (<a href="https://www.debian.org/doc/manuals/developers-reference/resources.html#pkg-tracking-system">docs</a>)
      <br />
      <input type="text" name="email" size="10" placeholder="your email" onblur="this.value=this.value.trim();"/>
      <select name="what">
	<option value="subscribe">sub</option>
	<option value="unsubscribe">unsub</option>
	<option value="advanced">opts</option>
      </select>
      <input type="submit" name="submit" value="go"/>
    </p>
  </form>
</xsl:template>

<xsl:template name="binary-packages">
  <div class="block binaries">
    <a name="binaries" />
    <h2>binaries</h2>
    <ul>
      <xsl:variable name="showshortdesc" select="count(binary/item) &lt; 5"/>
      <xsl:for-each select="binary/item">
	<xsl:sort select="text()"/>
	<xsl:variable name="pkg" select="text()"/>
	<xsl:variable name="tooltip"
		      select="concat($pkg, ': ',
			      $other/descriptions/shortdesc[@package=$pkg])" />
        <xsl:variable name="suite">
          <xsl:choose>
            <xsl:when test="$hasunstable">unstable/</xsl:when>
            <xsl:when test="$hasexperimental">experimental/</xsl:when>
            <xsl:when test="$hastesting">testing/</xsl:when>
            <xsl:when test="$hasstable">stable/</xsl:when>
            <xsl:when test="$hasoldstable">oldstable/</xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
	<li class="binpkg">
          <a class="binpkg" title="{$tooltip}"
             href="https://packages.debian.org/{$suite}{text()}">
	    <span class="binpkg"><xsl:value-of select="text()"/></span>
	  </a>
	  <span style="font-size: 70%">
	    (<a href="https://bugs.debian.org/{text()}"><xsl:value-of select="$other/bugs/item[@name=$pkg]/@all"/> bugs</a>: 
	    <xsl:element name="a">
	      <xsl:attribute name="title">critical, grave and serious</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=pkg&amp;data=</xsl:text>
		<xsl:call-template name="escape-name">
		  <xsl:with-param name="text"><xsl:value-of select="text()"/></xsl:with-param>
		</xsl:call-template>
		<xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=critical&amp;sev-inc=grave&amp;sev-inc=serious</xsl:text>
	      </xsl:attribute>
	      <xsl:value-of select="$other/bugs/item[@name=$pkg]/@rc"/>
	    </xsl:element>, 
	    <xsl:element name="a">
	      <xsl:attribute name="title">important and normal</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=pkg&amp;data=</xsl:text>
		<xsl:call-template name="escape-name">
		  <xsl:with-param name="text"><xsl:value-of select="text()"/></xsl:with-param>
		</xsl:call-template>
		<xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=important&amp;sev-inc=normal</xsl:text>
	      </xsl:attribute>
	      <xsl:value-of select="$other/bugs/item[@name=$pkg]/@normal"/>
	    </xsl:element>, 
	    <xsl:element name="a">
	      <xsl:attribute name="title">wishlist and minor</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=pkg&amp;data=</xsl:text>
		<xsl:call-template name="escape-name">
		  <xsl:with-param name="text"><xsl:value-of select="text()"/></xsl:with-param>
		</xsl:call-template>
		<xsl:text>&amp;archive=no&amp;pend-exc=pending-fixed&amp;pend-exc=fixed&amp;pend-exc=done&amp;sev-inc=minor&amp;sev-inc=wishlist</xsl:text>
	      </xsl:attribute>
	      <xsl:value-of select="$other/bugs/item[@name=$pkg]/@wishlist"/>
	    </xsl:element>, 
	    <xsl:element name="a">
	      <xsl:attribute name="title">pending and fixed</xsl:attribute>
	      <xsl:attribute name="href">
		<xsl:text>https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=pkg&amp;data=</xsl:text>
		<xsl:call-template name="escape-name">
		  <xsl:with-param name="text"><xsl:value-of select="text()"/></xsl:with-param>
		</xsl:call-template>
		<xsl:text>&amp;archive=no&amp;pend-inc=pending-fixed&amp;pend-inc=fixed</xsl:text>
	      </xsl:attribute>
	      <xsl:value-of select="$other/bugs/item[@name=$pkg]/@fixed"/>
	    </xsl:element>)
	  </span>
          <xsl:if test="$showshortdesc">
            <br/>
            <small>
              <abbrev title="{$other/descriptions/longdesc[@package=$pkg]}">
                <xsl:value-of select="$other/descriptions/shortdesc[@package=$pkg]"/>
              </abbrev>
            </small>
          </xsl:if>
	</li>
      </xsl:for-each>
    </ul>
  </div>
</xsl:template>

<xsl:template name="output-version">
  <xsl:param name="suite" select="''" />
  <xsl:param name="link" select="'yes'" />
  <xsl:variable name="version">
    <span class="srcversion" title="{$suite}">
      <xsl:value-of select="document(concat('../base/', $dir, '/', $suite,
			    '.xml'))/source/version"/>
    </span>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$link='yes'">
      <xsl:element name="a">
	<xsl:attribute name="class">dsc</xsl:attribute>
        <xsl:attribute name="href">
          <xsl:choose>
            <xsl:when test="starts-with($suite, 'security-')">
              <xsl:value-of select="$security-mirror"/>
              <xsl:text>/</xsl:text>
              <xsl:value-of
                 select="document(concat('../base/', $dir, '/',
                         $suite, '.xml'))/source/directory" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$mirror"/>
              <xsl:text>/</xsl:text>
              <xsl:value-of select="document(concat('../base/', $dir, '/',
                                    $suite, '.xml'))/source/directory" />
            </xsl:otherwise>
          </xsl:choose>
          <xsl:text>/</xsl:text>
          <xsl:value-of select="document(concat('../base/', $dir, '/',
                                $suite, '.xml'))/source/files/item[1]/filename"/>
	</xsl:attribute>
	<xsl:attribute name="title">.dsc, use dget on this link to retrieve source package</xsl:attribute>
	<img src="../common/save.png" alt="save" />
      </xsl:element>
      <xsl:choose>
        <xsl:when test="starts-with($suite, 'security-')">
          <xsl:element name="a">
            <xsl:attribute name="href">
              <xsl:text>https://packages.debian.org/source/</xsl:text>
              <xsl:value-of select="str:replace($suite, 'security-', '')" />
              <xsl:text>/</xsl:text>
              <xsl:copy-of select="$package" />
            </xsl:attribute>
            <xsl:copy-of select="$version" />
          </xsl:element>
        </xsl:when>
        <xsl:when test="'-proposed-updates' = substring($suite, string-length($suite)-string-length('-proposed-updates')+1)">
          <xsl:variable name="basesuite">
            <xsl:value-of select="str:replace($suite, '-proposed-updates', '')" />
          </xsl:variable>
          <a href="https://release.debian.org/proposed-updates/{$basesuite}.html#{$package}_{$version}">
            <xsl:copy-of select="$version" />
          </a>
        </xsl:when>
        <xsl:when test="str:replace($suite, '-updates', '') != $suite">
          <xsl:copy-of select="$version" />
        </xsl:when>
        <xsl:otherwise>
          <a href="https://packages.debian.org/source/{$suite}/{$package}">
            <xsl:copy-of select="$version" />
          </a>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy-of select="$version" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="more-info-marker">
  <xsl:param name="href" />
  <xsl:param name="title">more information are available on an external web page</xsl:param>
  <a href="{$href}">
    <img src="../common/external.png" title="{$title}" alt="..." />
  </a>
</xsl:template>

<xsl:template name="available-versions">
  <div class="block versions">
    <a name="versions" />
    <h2>versions
      <xsl:call-template name="more-info-marker">
	<xsl:with-param name="href">
	  <xsl:text>https://qa.debian.org/madison.php?package=</xsl:text>
	  <xsl:value-of select="$package" />
	</xsl:with-param>
	<xsl:with-param name="title">more versions can be listed by madison</xsl:with-param>
      </xsl:call-template>
      <xsl:text> </xsl:text>
      <xsl:call-template name="more-info-marker">
	<xsl:with-param name="href">http://snapshot.debian.org/package/<xsl:value-of select="package" />/</xsl:with-param>
	<xsl:with-param name="title">old versions available from snapshot.debian.org</xsl:with-param>
      </xsl:call-template>
      <a class="dsc" href="{$all-archs-mirror}/{$pooldir}">
	<img src="../common/folder.png" alt="pool" title="pool directory" />
      </a>
    </h2>

    <dl>
      <xsl:if test="$hasoldoldstable">  
	<dt title="old old stable release">o-o-stable</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldoldstable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hassecurity-oldoldstable">
	<dt title="security updates for the old old stable release">o-o-sec</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">security-oldoldstable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldoldstable-updates">
	<dt title="updates for the old old stable release">o-o-upd</dt>
	<dd>
      	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldoldstable-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldoldstable-proposed-updates">  
	<dt title="oldoldstable proposed updates">oos-p-u</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldoldstable-proposed-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldoldstable-backports">  
	<dt title="backports for the old old stable release">o-o-bpo</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldoldstable-backports</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldoldstable-backports-sloppy">  
	<dt title="newer backports for the old old stable release">o-o-bpo-slop</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldoldstable-backports-sloppy</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldstable">  
	<dt title="old stable release">oldstable</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldstable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hassecurity-oldstable">
	<dt title="security updates for the old stable release">old-sec</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">security-oldstable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldstable-updates">
	<dt title="updates for the old stable release">old-upd</dt>
	<dd>
      	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldstable-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldstable-proposed-updates">  
	<dt title="oldstable proposed updates">os-p-u</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldstable-proposed-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports">  
	<dt title="backports for the old stable release">old-bpo</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldstable-backports</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasoldstable-backports-sloppy">  
	<dt title="newer backports for the old stable release">old-bpo-slop</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">oldstable-backports-sloppy</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasstable">  
	<dt>stable</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">stable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hassecurity-stable">
	<dt title="security updates for the stable release">stable-sec</dt>
	<dd>
      	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">security-stable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasstable-updates">
	<dt title="updates for the stable release">stable-upd</dt>
	<dd>
      	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">stable-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasstable-proposed-updates">  
	<dt title="stable proposed updates">s-p-u</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">stable-proposed-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasstable-backports">
	<dt title="backports for the stable release">stable-bpo</dt>
	<dd>
      	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">stable-backports</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hastesting">
	<dt>testing</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">testing</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hassecurity-testing">
	<dt title="security updates for the testing release">testing-sec</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">security-testing</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hastesting-proposed-updates">
	<dt title="testing proposed updates">t-p-u</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">testing-proposed-updates</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasunstable">
	<dt>unstable</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">unstable</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasexperimental">
	<dt title="experimental release">exp</dt>
	<dd>
	  <xsl:call-template name="output-version">
	    <xsl:with-param name="suite">experimental</xsl:with-param>
	  </xsl:call-template>
	</dd>
      </xsl:if>
      <xsl:if test="$hasother and $other/@new_version">
	<dt title="waiting in the NEW queue for FTP master review">
	  <a href="https://ftp-master.debian.org/new.html">NEW</a>
	</dt>
	<dd>
	  <a href="https://ftp-master.debian.org/new/{$package}_{$other/@new_version}.html">
	    <xsl:value-of select="$other/@new_version" />
	  </a>
	</dd>
      </xsl:if>
      <xsl:if test="$other/@ubuntu='yes'">
        <dt><xsl:call-template name="ubuntu" /></dt>
        <dd>
          <a href="{$other/ubuntu/@url}"><xsl:value-of select="$other/ubuntu/@version"/></a>
        </dd>
      </xsl:if>
    </dl>
  </div>
</xsl:template>

<xsl:template name="ubuntu">
  <xsl:param name="text" select="'Ubuntu'" />
  <a href="https://wiki.ubuntu.com/Ubuntu/ForDebianDevelopers"
     title="Information about Ubuntu for Debian contributors">
    <xsl:value-of select="$text"/>
  </a>
</xsl:template>

<xsl:template name="output-patches-version">
  <xsl:param name="suite" select="''" />
  <xsl:param name="link" select="'yes'" />
  <xsl:variable name="version">
    <span class="srcversion" title="{$suite}">
      <xsl:value-of select="document(concat('../base/', $dir, '/', $suite,
			    '.xml'))/source/version"/>
    </span>
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$link='yes'">
      <a href="https://sources.debian.org/patches/{$package}/{$version}/">
        <xsl:copy-of select="$version" />
      </a>
    </xsl:when>
    <xsl:otherwise>
      <xsl:copy-of select="$version" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="patches">
  <xsl:if test="$hasother and ($other/@patches='yes' or $other/ubuntu/patch or $other/ubuntu/bugpatches or $other/@derivs='yes' or $other/bugs/@patch!='0')">
    <div class="block patches">
      <a name="patches" />
      <h2>patches</h2>
      <dl>
        <xsl:if test="$other/@patches='yes'">
          <dt><a href="https://sources.debian.org/patches/{$package}/">Debian</a></dt>
          <dd>
            <xsl:if test="$hasoldstable">
              <xsl:call-template name="output-patches-version">
                <xsl:with-param name="suite">oldstable</xsl:with-param>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test="$hasstable">
              <xsl:call-template name="output-patches-version">
                <xsl:with-param name="suite">stable</xsl:with-param>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test="$hastesting">
              <xsl:call-template name="output-patches-version">
                <xsl:with-param name="suite">testing</xsl:with-param>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test="$hasunstable">
              <xsl:call-template name="output-patches-version">
                <xsl:with-param name="suite">unstable</xsl:with-param>
              </xsl:call-template>
            </xsl:if>
            <xsl:if test="$hasexperimental">
              <xsl:call-template name="output-patches-version">
                <xsl:with-param name="suite">experimental</xsl:with-param>
              </xsl:call-template>
            </xsl:if>
          </dd>
        </xsl:if>
        <xsl:if test="0">
          <dt><a href="https://www.ports.debian.org/">Ports</a></dt>
          <dd>
            <a href="https://bugs.debian.org/779400">here</a>
          </dd>
        </xsl:if>
        <xsl:if test="0">
          <dt><a href="https://mentors.debian.net/">Mentors</a></dt>
          <dd>
            <a href="https://bugs.debian.org/779400">here</a>
          </dd>
        </xsl:if>
        <xsl:if test="$other/@ubuntu='yes' and $other/ubuntu/patch">
          <dt><xsl:call-template name="ubuntu" /></dt>
          <dd>
            <a href="{$other/ubuntu/patch/@url}" title="download patch file">
            <xsl:value-of select="$other/ubuntu/patch/@version"/></a>
            (<a href="https://ubuntudiff.debian.net/q/package/{$package}"><acronym title="UbuntuDiff: web based Ubuntu patch viewer">UD</acronym></a>)
          </dd>
        </xsl:if>
        <xsl:if test="$other/@derivs='yes'">
          <xsl:choose>
            <xsl:when test="$other/@ubuntu='yes' and $other/ubuntu/patch">
              <dt>Other <a href="https://www.debian.org/derivatives/"><abbrev title="derivatives">derivs</abbrev></a></dt>
            </xsl:when>
            <xsl:otherwise>
              <dt><a href="https://www.debian.org/derivatives/">Derivatives</a></dt>
            </xsl:otherwise>
          </xsl:choose>
          <dd>
            <a href="http://deriv.debian.net/patches/{$hash}/{$package}/">here</a>
          </dd>
        </xsl:if>
        <xsl:if test="0">
          <dt>Other <a href="https://wiki.debian.org/Services/distromatch"><abbrev title="distributions">distros</abbrev></a></dt>
          <dd>
            <a href="https://wiki.debian.org/Services/distromatch">here</a>
          </dd>
        </xsl:if>
        <xsl:if test="$other/bugs/@patch!='0'">
          <dt>Debian proposed</dt>
          <dd>
            <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?src={$escaped-package}&amp;include=tags:patch&amp;exclude=tags:pending&amp;pend-exc=done&amp;repeatmerged=no">
              <xsl:value-of select="$other/bugs/@patch"/>
            </a>
            <xsl:if test="$other/bugs/@patch_m">
              <xsl:text> (</xsl:text>
              <a title="counting merged bugs" href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?src={$escaped-package}&amp;include=tags:patch&amp;exclude=tags:pending&amp;pend-exc=done&amp;repeatmerged=yes">
                <xsl:value-of select="$other/bugs/@patch_m"/>
              </a>
              <xsl:text>)</xsl:text>
            </xsl:if>
          </dd>
        </xsl:if>
        <xsl:if test="$other/@ubuntu='yes' and $other/ubuntu/bugpatches">
          <dt><xsl:call-template name="ubuntu"/> proposed</dt>
          <dd>
            <a href="{$other/ubuntu/bugpatches/@url}">
            <xsl:value-of select="$other/ubuntu/bugpatches/@count"/></a>
          </dd>
        </xsl:if>
        <xsl:if test="0">
          <dt><a href="">Upstream</a> proposed</dt>
          <dd>
            <a href="https://bugs.debian.org/779400">here</a>
          </dd>
        </xsl:if>
      </dl>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="other-links">
  <div class="block links">
    <a name="links" />
    <h2>links</h2>
    <ul>
      <xsl:if test="homepage">
        <li><a title="upstream web homepage" href="{homepage}">homepage</a></li>
      </xsl:if>
<!--
      #FIXME: re-enable and report a tracker.d.o bug when this is updated again
      <xsl:if test="$other/@upstreaminfo='yes'">
	<li>
          <xsl:variable name="upstream_url"><xsl:call-template name="mk_upstream_url" /></xsl:variable>
	  <a title="More information about the upstream project" href="{$upstream_url}">upstream info</a>
	</li>
      </xsl:if>
-->
      <li>
	<xsl:variable name="directory"><xsl:value-of select="directory"/></xsl:variable>
	<xsl:variable name="suite">
	  <xsl:choose>
	      <xsl:when test="$hasunstable">unstable</xsl:when>
	      <xsl:when test="$hasexperimental">experimental</xsl:when>
	      <xsl:when test="$hastesting">testing</xsl:when>
	      <xsl:when test="$hasstable">stable</xsl:when>
	      <xsl:when test="$hasoldstable">oldstable</xsl:when>
	  </xsl:choose>
	</xsl:variable>
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>http://metadata.ftp-master.debian.org/changelogs/</xsl:text>
            <xsl:choose>
              <xsl:when test="starts-with($directory, 'pool/updates/')">
                <xsl:value-of select="substring-after($directory,'pool/updates/')"/>
              </xsl:when>
              <xsl:when test="starts-with($directory, 'pool/')">
                <xsl:value-of select="substring-after($directory,'pool/')"/>
              </xsl:when>
            </xsl:choose>
	    <xsl:text>/</xsl:text>
	    <xsl:value-of select="$suite"/>
	    <xsl:text>_changelog</xsl:text>
	  </xsl:attribute>
	  <xsl:text>changelog</xsl:text>
	</xsl:element>
	/
	<xsl:element name="a">
	  <xsl:attribute name="href">
	    <xsl:text>http://metadata.ftp-master.debian.org/changelogs/</xsl:text>
            <xsl:choose>
              <xsl:when test="starts-with($directory, 'pool/updates/')">
                <xsl:value-of select="substring-after($directory,'pool/updates/')"/>
              </xsl:when>
              <xsl:when test="starts-with($directory, 'pool/')">
                <xsl:value-of select="substring-after($directory,'pool/')"/>
              </xsl:when>
            </xsl:choose>
	    <xsl:text>/</xsl:text>
	    <xsl:value-of select="$suite"/>
            <xsl:text>_copyright</xsl:text>
	  </xsl:attribute>
	  <xsl:text>copyright</xsl:text>
	</xsl:element>
      </li>
	<li>
        <xsl:variable name="suite">
            <xsl:choose>
              <xsl:when test="$hasunstable">unstable</xsl:when>
              <xsl:when test="$hasexperimental">experimental</xsl:when>
              <xsl:when test="$hastesting">testing</xsl:when>
              <xsl:when test="$hasstable">stable</xsl:when>
              <xsl:when test="$hasoldstable">oldstable</xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:text>buildd: </xsl:text>
          <a href="https://buildd.debian.org/status/package.php?p={$escaped-package}&amp;suite={$suite}"
             title="buildd.debian.org build logs">logs</a>
	       <xsl:if test="$hasexperimental and $hasunstable">
		 <xsl:text>, </xsl:text>
                 <a href="https://buildd.debian.org/status/package.php?p={$escaped-package}&amp;suite=experimental"
		    title="experimental build logs">exp</a>
	       </xsl:if>
               <xsl:if test="$other/@logcheck='yes'">
	         <xsl:variable name="logcheck_url"><xsl:call-template name="mk_logcheck_url" /></xsl:variable>
                 <xsl:text>, </xsl:text>
                 <a title="report about issues spotted in buildd logs" href="{$logcheck_url}">checks</a>
               </xsl:if>
               <xsl:text>, </xsl:text>
               <a title="clang compiler build log" href="http://clang.debian.net/pts.php?p={$escaped-package}&amp;format=html">clang</a>
	</li>
      <xsl:if test="$hasother and $other/@piuparts='yes'">
	<li>
	  <a title="report about errors found while stressing package installation"
	     href="https://piuparts.debian.org/sid/source/{$hash}/{$package}.html">piuparts</a>
	</li>
      </xsl:if>
      <xsl:if test="($hasunstable and $other/debcheck/@unstable='yes')
		    or ($hastesting and $other/debcheck/@testing='yes')
		    or ($hasstable and $other/debcheck/@stable='yes')">
	<li>
	  debcheck:
	  <xsl:if test="$hasunstable">
	    <xsl:text> </xsl:text>
            <a href="https://qa.debian.org/debcheck.php?dist=unstable&amp;package={$escaped-package}">unstable</a>
	  </xsl:if>
	  <xsl:if test="$hastesting">
	    <xsl:text> </xsl:text>
            <a href="https://qa.debian.org/debcheck.php?dist=testing&amp;package={$escaped-package}">testing</a>
	  </xsl:if>
	  <xsl:if test="$hasstable">
	    <xsl:text> </xsl:text>
            <a href="https://qa.debian.org/debcheck.php?dist=stable&amp;package={$escaped-package}">stable</a>
	  </xsl:if>
	</li>
      </xsl:if>
      <xsl:if test="$other/@testing='yes'">
	<li>
	  <a title="as-installed package tests {$other/testing/@status}" href="https://ci.debian.net/packages/{$hash}/{$escaped-package}/">tests</a>
	  (<a title="as-installed package tests log file"
	    href="https://ci.debian.net/data/packages/unstable/amd64/{$hash}/{$escaped-package}/latest-autopkgtest/log.gz">log</a>)
	</li>
      </xsl:if>
      <xsl:if test="$other/@lintian='yes'">
	<xsl:variable name="lintian_url"><xsl:call-template name="mk_lintian_url" /></xsl:variable>
	<li>
	  <a title="report about packaging issues spotted by lintian"
	     href="{$lintian_url}">lintian</a>
	  <xsl:if test="$lin_errs + $lin_warns > 0">
	    <xsl:text> </xsl:text>
	    <small>
              (<span id="lintian_errors" title="errors"><xsl:value-of select="$lin_errs" /></span>,
	      <span id="lintian_warnings" title="warnings"><xsl:value-of select="$lin_warns" /></span>)
            </small>
	  </xsl:if>
	</li>
      </xsl:if>
      <li>
        <a title="package popularity"
	   href="https://qa.debian.org/popcon.php?package={$escaped-package}">popcon</a>
      </li>
      <xsl:if test="$other/@svnbuildstat='yes'">
        <li>
          <a href="http://svnbuildstat.debian.net/packages/info/{$escaped-package}">Svnbuildstat</a>
        </li>
      </xsl:if>
      <xsl:if test="$other/i18n/@href">
	<li>
	  <a title="translation status"
	     href="{$other/i18n/@href}">l10n</a>
	  <small>
	    (<span title="completeness of Debian string translation">
	      <xsl:value-of select="$other/i18n/@deb" />
	    </span>,
	    <span title="completeness of non-Debian string translation">
	      <xsl:value-of select="$other/i18n/@nondeb" />
	    </span>)
	  </small>
	</li>
      </xsl:if>
      <xsl:if test="$other/@fonts='yes'">
	<li>
	  <a title="fonts review" href="{$other/fonts/@href}">fonts</a>
	</li>
      </xsl:if>
      <li>
        <xsl:variable name="escaped-email">
          <xsl:call-template name="escape-name">
            <xsl:with-param name="text"><xsl:value-of select="maintainer/email"/></xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <a title="edit all debtags" href="https://debtags.debian.org/rep/todo/maint/{$escaped-email}#{$escaped-package}">debtags</a>
      </li>
      <!-- FIXME: change this to per-source-package links when available -->
      <xsl:choose>
        <xsl:when test="count($other/screenshots/package)>1">
	  <li>
	    screenshots:
            <xsl:for-each select="$other/screenshots/package">
              <a title="screenshots for {@name}" href="https://screenshots.debian.net/package/{@name}">
                <xsl:value-of select="position()"/>
              </a>
              <xsl:if test="position()!=last()"><xsl:text>, </xsl:text></xsl:if>
            </xsl:for-each>
	  </li>
        </xsl:when>
        <xsl:when test="count($other/screenshots/package)=1">
          <li>
            <a href="https://screenshots.debian.net/package/{$other/screenshots/package[1]/@name}">screenshots</a>
          </li>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="$other/@dedup='yes'">
	<li>
	  <a title="duplicate and similar files" href="https://dedup.debian.net/source/{$package}">dedup</a>
	</li>
      </xsl:if>
      <li>
	<img src="../common/rdf.png" alt="rdf" /> <a title="Alternate RDF meta-data (Turtle)" href="{$package}.ttl">RDF meta-data</a>
      </li>
      <xsl:if test="$hasexperimental or $hasunstable or $hastesting or $hasstable or $hasoldstable">
	<xsl:variable name="suite">
	  <xsl:choose>
	      <xsl:when test="$hasunstable">unstable</xsl:when>
	      <xsl:when test="$hasexperimental">experimental</xsl:when>
	      <xsl:when test="$hastesting">testing</xsl:when>
	      <xsl:when test="$hasstable">stable</xsl:when>
	      <xsl:when test="$hasoldstable">oldstable</xsl:when>
	  </xsl:choose>
	</xsl:variable>
        <li>
          <a href="https://sources.debian.org/src/{$package}/{$suite}/">browse source code</a>
        </li>
      </xsl:if>
      <xsl:if test="$hasunstable">
        <li>
          <form method="get" action="/cgi-bin/codesearch.cgi">
            <p>
              <input type="hidden" name="package" value="{$package}"/>
              <input type="text" name="q" placeholder="search source code" class="codesearch"/>
            </p>
          </form>
        </li>
      </xsl:if>
    </ul>
  </div>
</xsl:template>

<xsl:template name="todo-list">
  <xsl:variable name="todo">
    <xsl:call-template name="issue-security" />
    <xsl:call-template name="issue-nmu" />
    <xsl:call-template name="issue-lintian" />
    <xsl:call-template name="issue-logcheck" />
    <xsl:call-template name="issue-mentors-pending" />
    <xsl:call-template name="issue-comaintenance" />
    <xsl:call-template name="issue-outdate-stdver" />
    <xsl:call-template name="issue-new-upstream" />
    <xsl:call-template name="issue-patches" />
    <xsl:call-template name="issue-l10n" />
    <xsl:call-template name="issue-release-goals" />
    <xsl:call-template name="issue-watchbroken" />
    <xsl:call-template name="issue-watchavail" />
    <xsl:call-template name="issue-depneedsmaint" />
    <xsl:call-template name="issue-dedup" />
    <xsl:if test="$hasother">
      <xsl:for-each select="$other/todo/item">
	<xsl:call-template name="outputitem" />
      </xsl:for-each>
    </xsl:if>
  </xsl:variable>

  <xsl:if test="count($todo)>0 and string($todo)!=''">  
    <div class="block todo">
      <a name="todo" />
      <h2>todo</h2>
      <ul>
	<xsl:copy-of select="$todo" />
      </ul>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="problems">
  <xsl:variable name="problems">
    <xsl:call-template name="issue-testing-excuses">
      <xsl:with-param name="section" select="section" />
    </xsl:call-template>
    <xsl:call-template name="issue-piuparts" />
    <xsl:call-template name="issue-testing-failed" />
    <xsl:call-template name="issue-ancient-stdver" />
    <xsl:call-template name="issue-item-dead-package" />
    <xsl:call-template name="issue-item-override-disparity" />
    <xsl:call-template name="issue-item-help-bugs" />
    <xsl:call-template name="issue-item-wnpp" />
    <!-- <xsl:call-template name="issue-item-watch-failure" /> -->
    <xsl:call-template name="issue-item-dehs-failure" />
    <xsl:call-template name='issue-urls'>
      <xsl:with-param name="email" select="maintainer/email" />
    </xsl:call-template>
    <xsl:if test="$hasother">
      <xsl:for-each select="$other/problems/item">
	<xsl:call-template name="outputitem"/>
      </xsl:for-each>
    </xsl:if>
  </xsl:variable>

  <xsl:if test="count($problems)>0 and string($problems)!=''">
    <div class="block problems">
      <a name="problems" />
      <h2>problems</h2>
      <ul>
	<xsl:copy-of select="$problems" />
      </ul>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="autoremoval-status">
  <xsl:if test="$other/@autoremoval='yes'">
    <div class="block autoremoval">
      <a name="autoremoval" />
      <h2>autoremoval from testing</h2>
      <ul>
        <li>
        Version <xsl:value-of select="$other/autoremoval/@version"/> of <xsl:value-of select="$package"/> is marked for autoremoval from testing
        <xsl:value-of select="$other/autoremoval/@removaldate"/>.
        </li>
        <xsl:if test="$other/autoremoval/@bugs='yes'">
          <li>
            It is affected by RC
            <xsl:if test="count($other/autoremoval/bug)>1">bugs</xsl:if>
            <xsl:if test="count($other/autoremoval/bug)=1">bug</xsl:if>
            <xsl:for-each select="$other/autoremoval/bug">
              <xsl:if test="position()=last() and position() != 1"><xsl:text> and </xsl:text>
              </xsl:if>
              <xsl:if test="position()!=last() and position() != 1"><xsl:text>, </xsl:text>
              </xsl:if>
              #<a href="https://bugs.debian.org/{@bugid}"><xsl:value-of select="@bugid"/></a>
            </xsl:for-each>.
          </li>
        </xsl:if>
        <xsl:if test="$other/autoremoval/@buggy_dep='yes'">
          <li>
            It depends (transitively) on
            <xsl:for-each select="$other/autoremoval/buggy_dep">
              <xsl:if test="position()=last()"><xsl:text> and </xsl:text>
              </xsl:if>
              <xsl:if test="position()!=last() and position() != 1"><xsl:text>, </xsl:text>
              </xsl:if>
              <a href="/{@pkg}"><xsl:value-of select="@pkg"/></a>
            </xsl:for-each>,
            affected by RC bug(s)
            <xsl:for-each select="$other/autoremoval/bug_dep">
              <xsl:if test="position()=last()"><xsl:text> and </xsl:text>
              </xsl:if>
              <xsl:if test="position()!=last() and position() != 1"><xsl:text>, </xsl:text>
              </xsl:if>
              <a href="https://bugs.debian.org/{@bugid}"><xsl:value-of select="@bugid"/></a>
            </xsl:for-each>
          </li>
        </xsl:if>
        <xsl:if test="$other/autoremoval/@rdep='yes'">
          <li>
            The removal of <xsl:value-of select="$package"/> will also cause the removal of
            (transitive) reverse dependencies:
            <xsl:for-each select="$other/autoremoval/rdep">
              <a href="/{@pkg}"><xsl:value-of select="@pkg"/></a>
              <xsl:text> </xsl:text>
            </xsl:for-each>
          </li>
        </xsl:if>
      <li>You should try to prevent the removal from testing by fixing these bugs.
      </li>
      </ul>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="testing-status">
  <xsl:if test="$hasexcuse or $other/@transitions='yes'">
    <div class="block testing">
      <a name="testing" />
      <h2>testing migration</h2>
      <xsl:if test="$other/@transitions='yes'">
	<div class="warning">
	  <ul>
	    <xsl:for-each select="$other/transitions/transition">
	      <li>
	      <xsl:choose>
	        <xsl:when test="@status='planned'">
                  This package will soon be part of the <a href="https://release.debian.org/transitions/html/{@name}.html"><xsl:value-of select="@name" /></a> transition. You might want to ensure that your package is ready for it.
		</xsl:when>
	        <xsl:when test="@status='ongoing'">
                  This package is part of the ongoing testing transition known as <a href="https://release.debian.org/transitions/html/{@name}.html"><xsl:value-of select="@name" /></a>.
		  <xsl:if test="@reject!='yes'">
		  Please avoid uploads unrelated to this transition, they would
		  likely delay it and require supplementary work from the release
		  managers. On the other hand, if your package has problems
		  preventing it to migrate to testing, please fix them
		  as soon as possible.
		  </xsl:if>
	        </xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	      </xsl:choose>
              You can probably find supplementary information in the
	      <a href="https://lists.debian.org/debian-release/">debian-release
	      archives</a> or in the corresponding
	      <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=transition">release.debian.org
	      bug</a>.
	      <xsl:if test="@reject='yes'">
		<strong>Uploads to unstable will be rejected</strong> while
		the transition is ongoing; you might want to upload to
		experimental in the meantime, or
		contact <tt><a href="mailto:debian-release@lists.debian.org">debian-release</a></tt>
		if an upload is really necessary.

	      </xsl:if>
	      </li>
	    </xsl:for-each>
	  </ul>
	</div>
      </xsl:if>
      <xsl:if test="$hasexcuse">
        <a title="reasons why the package is not moving to testing"
           href="https://qa.debian.org/excuses.php?package={$escaped-package}">excuses</a>:
	<ul class="testing-excuses">
	  <xsl:for-each select="document(concat('../base/', $dir, '/excuse.xml'))/excuse/item">
	    <xsl:call-template name="outputitem"/>
	  </xsl:for-each>
	</ul>
      </xsl:if>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="static-info">
  <xsl:call-template name="output-static">
    <xsl:with-param name="static" select="$static" />
  </xsl:call-template>
</xsl:template>

<xsl:template name="latest-news">
  <xsl:call-template name="output-news">
    <xsl:with-param name="news" select="$news" />
  </xsl:call-template>
</xsl:template>

<xsl:variable name="static">
  <xsl:if test="$hasnews">
    <xsl:for-each select="document(concat('../base/', $dir, '/news.xml'))/news/static/item">
      <xsl:call-template name="outputitem"/>
    </xsl:for-each>
  </xsl:if>
</xsl:variable>

<xsl:variable name="news">
  <xsl:if test="$hasnews">
    <xsl:for-each select="document(concat('../base/', $dir, '/news.xml'))/news/news/item">
      <xsl:call-template name="outputitem"/>
    </xsl:for-each>
  </xsl:if>
</xsl:variable>

<!-- All the work is done in a single template -->
<xsl:template match="source">

  <!-- Start of html -->
  <html>
    <head>
      <meta name="ROBOTS" content="NOFOLLOW"/>
      <link type="text/css" title="User selected" rel="stylesheet" href="../common/default.css"/>
      <link type="text/css" title="Official" rel="alternate stylesheet" href="../common/revamp.css"/>
      <link type="text/css" title="Legacy PTS" rel="alternate stylesheet" href="../common/pts.css"/>
      <link type="text/css" title="Compact rendering" rel="alternate stylesheet" href="../common/compact.css"/>
      <link type="application/opensearchdescription+xml" title="Debian PTS search" rel="search" href="../common/search.xml"/>
      <script type="text/javascript" src="../common/pts.js"></script>
      <xsl:if test="count($news)>0 and string($news)!=''">
	<link rel="alternate" type="application/rss+xml" title="RSS"
	      href="{$package}/news.rss20.xml" />
      </xsl:if>
      <!-- Link to the RDF alternate representations -->
      <link rel="alternate" type="text/turtle" href="https://packages.qa.debian.org/{$dir}.ttl" />
      <link rel="alternate" type="application/rdf+xml" href="https://packages.qa.debian.org/{$dir}.rdf" />
      <title>Debian Package Tracking System -
	<xsl:value-of select="$package"/></title>
    </head>
    <body onload="javascript:onLoad();">
      <div class="quickform" style="float: right;">
	<form title="jump to the PTS page of another source package"
	      method="get" action="/common/index.html" onsubmit="return packageSubmit(this);">
	  <p>
	    <input type="text" size="14" name="src" value="" onblur="this.value=this.value.trim();"/>
	    <input type="submit" value="jump to" />
	  </p>
	</form>
      </div>

      <h1>
	<xsl:value-of select="$package"/><br />
        <xsl:choose>
          <xsl:when test="count($other/descriptions/shortdesc)=1">
            <small><xsl:value-of select="$other/descriptions/shortdesc[1]" /></small>
          </xsl:when>
          <xsl:when test="$other/descriptions/shortdesc[@package=$package]">
            <small><xsl:value-of select="$other/descriptions/shortdesc[@package=$package]" /></small>
          </xsl:when>
          <xsl:otherwise><small>source package</small></xsl:otherwise>
        </xsl:choose>
      </h1>
      <div style="margin: 5px; color: red; font-weight: bold; text-align: center;">NEW: Take a look at the new package tracker: <a href="https://tracker.debian.org/pkg/{$package}">tracker.debian.org/pkg/<xsl:value-of select="$package"/></a></div>

      <div id="body">
	<xsl:choose>
	  <xsl:when test="$removed='yes'">
	    <div class="block removed">
	      <p>This package is not part of any Debian
		distribution. Thus you won't find much information
		here. The package is either very new and hasn't
		appeared on mirrors yet, or it's an old package that
		eventually got removed.  The old news are kept for
		historic purpose only.</p>
	      <xsl:call-template name="static-info" />
	      <xsl:call-template name="latest-news" />
	    </div>
	  </xsl:when>
	  <xsl:otherwise>	<!-- non removed package -->
	    <div class="left maincol">
	      <xsl:call-template name="general-information" />
	      <xsl:call-template name="available-versions" />
	      <xsl:call-template name="binary-packages" />
	    </div>
	    <div class="center maincol">
	      <xsl:call-template name="todo-list" />
	      <xsl:call-template name="problems" />
	      <xsl:call-template name="autoremoval-status" />
	      <xsl:call-template name="testing-status" />
	      <xsl:call-template name="static-info" />
	      <xsl:call-template name="latest-news" />
	    </div>
	    <div class="right maincol">
	      <xsl:call-template name="bugs-count" />
	      <xsl:call-template name="other-links" />
	      <xsl:call-template name="patches" />
	    </div>
	  </xsl:otherwise>
	</xsl:choose>

	<hr/>
	<div class="footer">
          <table width="100%">
            <tr>
	      <td>
              <!--
		<div class="quickform">
		  <form id="csspref-form" method="get"
			action="/common/set-csspref.php">
		    <p>
		      change skin:
		      <select name="csspref" onchange="javascript:onChangeStyle();">
			<option value="revamp.css">default</option>
			<option value="compact.css">compact</option>
			<option value="pts.css">legacy</option>
		      </select>
		    </p>
		  </form>
		</div>
              -->
		<div class="quickform">
		  <xsl:call-template name="pts-subscription" />
		</div>
	      </td>
              <td>
                <p>
                  <em><a href="https://www.debian.org">Debian</a>
                    Package Tracking System</em> - Copyright ©
                    2002-2014 Raphaël Hertzog, Stefano Zacchiroli and
                    others.<br/> Report problems to the
                  <a href="https://bugs.debian.org/qa.debian.org"><tt>qa.debian.org</tt>
                    pseudopackage</a> in the <a href="https://bugs.debian.org">Debian
                    <acronym title="Bug Tracking System">BTS</acronym></a>.<br/>
                    git revision <xsl:value-of select="$gitrev"/> | Checkout or browse
                    <a href="https://salsa.debian.org/qa/pts">git repository</a><br/>
                  Last modified: <xsl:value-of select="$date"/>.
                </p>
              </td>
            </tr>
          </table>
	</div>
      </div>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
