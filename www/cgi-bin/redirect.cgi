#!/usr/bin/python

# Copyright 2013 Paul Wise
# Available under the terms of the General Public License version 2
# or (at your option) any later version.

import cgi, urllib
#import cgitb; cgitb.enable()

form = cgi.FieldStorage()
src = form.getvalue('src', '').strip()
ext = form.getvalue('ext', 'html').strip()

if src:
	pre = src[:4] if src.startswith('lib') else src[0]
	url = '/%s/%s.%s' % (pre, src, ext)
else:
	url = '/common/index.html'

print 'Status: 302 Found'
print 'Location: %s' % url
print 'Content-Type: text/plain'
print
print 'Please see %s' % url
