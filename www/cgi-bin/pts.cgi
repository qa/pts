#!/usr/bin/python
# -*- coding: utf8 -*-

# vim: expandtab

# Copyright 2002-2010 Raphaël Hertzog
# Available under the terms of the General Public License version 2
# or (at your option) any later version.

import cgi, urllib, sys, smtplib, string
#import cgitb; cgitb.enable()

form = cgi.FieldStorage()
pkg = ""
addr = ""
what = ""
advanced = ""
if form.has_key("package"): pkg = form["package"].value
if form.has_key("email"): addr = form["email"].value
if form.has_key("what"): what = form["what"].value
if form.has_key("advanced"): advanced = form["advanced"].value
command = ""

# Cleanup input
pkg = pkg.replace("<","").replace(">", "").replace("\"", "").replace("'", "")
addr = addr.replace("<","").replace(">", "").replace("\"", "").replace("'", "")
what = what.replace("<","").replace(">", "").replace("\"", "").replace("'", "")
advanced = advanced.replace("<","").replace(">", "").replace("\"", "").replace("'", "")
pkg = pkg.strip()
addr = addr.strip()
what = what.strip()
advanced = advanced.strip()

# Very basic check of email
if string.find(addr, "@") == -1:
    print "Content-Type: text/html"
    print
    print "<html><body><h2 style='color:red'>The submitted email address is invalid : " + addr + "</h2></body></html>"
    sys.exit(0)

if what == "subscribe":
    command = "subscribe %s %s\n" % (pkg, addr)
elif what == "unsubscribe":
    command = "unsubscribe %s %s\n" % (pkg, addr)
elif what == "advanced":
    f = open("subscription.html", "r")
    print "Content-Type: text/html"
    print
    while 1:
        line = f.readline()
        if not line: break #eof
        line = string.replace(line,"#PKG#", pkg)
        line = string.replace(line, "#EMAIL#", addr)
        print line,
    sys.exit(0)
else:
    sys.exit(1)

if advanced:
    # Add all tags treatment
    command = command + "keyword %s %s =" % (pkg, addr)
    for tag in ["bts", "bts-control", "upload-source", "upload-binary", "archive", "summary", "default", "vcs", "translation", "derivatives", "derivatives-bugs", "contact", "build"]:
        choice = ""
        if form.has_key("kw_" + tag): choice = form["kw_" + tag].value
        if choice:
            command = command + " " + tag
    command = command + "\n"

msg = """To: control@tracker.debian.org
Subject: Subscription for %s package

%s""" % (pkg, command)

print "Content-Type: text/html"
print
print "<html><head><title>Command to send</title></head><body>"
print "<h1>Send this mail to finish the process</h1>"
print "<p>To finish the process, you have to send the following mail by yourself:</p>"
print "<blockquote><pre>"
print cgi.escape(msg)
print "</pre></blockquote>"
print "<p>Once done, you will soon receive a confirmation mail, please read it carefully.</p>"
print "<h1>Or use the new package tracker</h1>"
print "<p>Alternatively, you can use the new package tracker to subscribe through "
print "its web interface. You will have to create an account first if you don't "
print "have any. <a href='https://tracker.debian.org/pkg/$pkg'>Click here</a> "
print "if you are interested.</p>"
print "</body></html>"

