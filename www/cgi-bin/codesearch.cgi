#!/usr/bin/python

# Copyright 2013 Paul Wise
# Available under the terms of the General Public License version 2
# or (at your option) any later version.

import cgi, urllib
#import cgitb; cgitb.enable()

q = ''
cs = 'https://codesearch.debian.net/search?'
fields = {
	'package': True,
	'suite': True,
	'q': False,
}

form = cgi.FieldStorage()
for field, prefix in fields.items():
	if field in form and form[field]:
		if q:
			q += ' '
		if prefix:
			q += '%s:%s' % (field, form[field].value)
		else:
			q += '%s' % form[field].value

url = cs + urllib.urlencode({'q': q})

print 'Location: %s' % url
print 'Content-Type: text/plain'
print
print 'Please see %s' % url
