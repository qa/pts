Use common-debian-service-https-redirect * packages.qa.debian.org

<VirtualHost *:443>
    ServerName packages.qa.debian.org
    ServerAdmin owner@packages.qa.debian.org
    DocumentRoot /srv/packages.qa.debian.org/www/web

    <Directory /srv/packages.qa.debian.org/www/web>
        Require all granted
        Options Indexes FollowSymLinks
    </Directory>

    Use common-debian-service-ssl packages.qa.debian.org
    Use common-ssl-HSTS

    ErrorLog /var/log/apache2/packages.qa.debian.org-error.log
    CustomLog /var/log/apache2/packages.qa.debian.org-access.log privacy

    # All our html pages are UTF-8
    AddType "text/html; charset=UTF-8" html

    AddType text/turtle .ttl
    AddCharset UTF-8 .ttl

    ScriptAlias /cgi-bin /srv/packages.qa.debian.org/www/cgi-bin
    <Directory /srv/packages.qa.debian.org/www/cgi-bin>
      Require all granted
      Options ExecCGI
    </Directory>
    ErrorDocument 404 /cgi-bin/error404.cgi

    RewriteEngine on
    RewriteCond %{HTTP_COOKIE} csspref=^(compact\.css|pts\.css|revamp\.css)$
    RewriteRule ^/common/default\.css$ /common/%1 [L]
    RewriteRule ^/common/default\.css$ /common/revamp.css

    # Distinguish between RDF or HTML serving
    RewriteCond %{HTTP_ACCEPT} !(application/rdf\+xml)
    # If the Accept header doesn't request RDF+XML, then skip the following 2 lines
    RewriteRule .* - [S=2]
    RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.rdf [L,R]
    RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.rdf [L,R]

    RewriteCond %{HTTP_ACCEPT} !(text/turtle)
    # If the Accept header doesn't request Turtle, then skip the following 2 lines
    RewriteRule .* - [S=2]
    RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.ttl [L,R]
    RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.ttl [L,R]

    RewriteRule ^/$ /common/index.html [L,R]
    RewriteRule ^/favicon\.ico$ https://www.debian.org/favicon.ico [L,R]
    RewriteRule ^/\s*lib([^/\s])([^/\s]+)\s*$ /lib$1/lib$1$2.html [L,R]
    RewriteRule ^/\s*([^/\s])([^/\s]+)\s*$ /$1/$1$2.html [L,R]

    RewriteCond %{QUERY_STRING} ^src=
    RewriteRule ^/common/index.html$ /cgi-bin/redirect.cgi [L,PT]
    RewriteCond %{QUERY_STRING} ^srcrdf=(.*)
    RewriteRule ^/common/RDF.html$ /cgi-bin/redirect.cgi?ext=ttl&src=%1 [L,PT]
</VirtualHost>

