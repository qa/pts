package ConfirmationSpool;

# Copyright 2002 Raphaël Hertzog <hertzog@debian.org>
# Available under the terms of the General Public License version 2
# or (at your option) any later version

use Digest::MD5 qw(md5_hex);

use strict;

=head1 ConfirmationSpool

A confirmation spool is a set of strings that are waiting a confirmation
in the form of a key. Usually the key is used to authenticate a mail
adress. You get a command with an email adress, and you want to be sure
that the email is not forged. You send a confirmation mail indicating
to respond with the given "key" (which should not be predictable). Once
you get the key back, you're sure that the email is not forged and that
the person concerned accepted the command that was sent.

=head2 my $cs = ConfirmationSpool->new($spooldir)

When you create a confirmation spool, you need to indicate a directory
in which the strings (mails) waiting a confirmation will be stored.

=cut
sub new {
    my $type = shift;
    my $class = ref($type) || $type;
    my $directory = shift;

    die "$directory is not a directory: $!\n" if (! -d $directory);

    my $self = { "dir" => $directory, "sendmail" => "/usr/sbin/sendmail" };

    return bless $self, $class;
}

=head2 $cs->set_confirmation_template($fileorstring)

Set the file used as a template to ask for a confirmation of something.
@CMD@ will be automatically replaced by the string to authenticate. @KEY@
will be replaced by the key that the user has to send back to authenticate
the command.

If the file doesn't exist, the param is considered as a string containing
the confirmation template.

The template must be a complete mail ready to be piped into "sendmail -t".

=cut
sub set_confirmation_template {
    my ($self, $templ) = @_;
    my $res = "";
    if (-f $templ) {
	open(TEMPLATE, "< $templ") || die "Can't open $templ : $!\n";
	while (defined($_ = <TEMPLATE>)) {
	    $res .= $_;
	}
	close(TEMPLATE);
    } else {
	$res = $templ;
    }
    $self->{'template'} = $res;
}

=head2 $key = $cs->ask_confirmation($email, $string)

Add a string to be confirmed. Send a confirmation mail (it uses
the template set with set_confirmation_template)

=cut
sub ask_confirmation {
    my ($self, $email, $string, $subst) = @_;

    $subst = { } if (! defined $subst);
    
    # Generate a unique code
    srand(time());
    my @trans = ('A' .. 'Z', 'a' .. 'z', 0 .. 9);
    my $nb = scalar @trans;
    my @rand;
    for(my $i = 0; $i < 16; $i++) { push @rand, int(rand($nb)) }
    my $key = md5_hex(join("", $string, time(), map { $trans[$_] } @rand));

    # Store the string with the given key
    open(SPOOL, "> $self->{'dir'}/$key") || 
	die "Can't write spool file: $!\n";
    print SPOOL $string;
    close SPOOL;

    # Send the confirmation message
    my $msg = $self->{'template'};
    $msg =~ s/\@EMAIL\@/$email/g;
    $msg =~ s/\@KEY\@/$key/g;
    foreach my $varname (keys %{$subst}) {
	my $name = uc($varname);
	$msg =~ s/\@$name\@/$subst->{$varname}/g;
    }
    open(MAIL, "| $self->{'sendmail'} -oi -t") 
	or die "Can't fork sendmail: $!\n";
    print MAIL $msg;
    close MAIL or die "Problem happened while sending mail: $!\n";
}

=head2 $string = $cs->confirm($key)

Verify that the key confirms a previously sent command. Returns the
command if ok, undef otherwise.

=cut
sub confirm {
    my ($self, $key) = @_;

    # Check in the spool
    if (! -f "$self->{'dir'}/$key") {
	return undef;
    }

    # Return the content of the file
    my $msg;
    open(SPOOL, "< $self->{'dir'}/$key") || die "Can't open $key file: $!\n";
    while (defined($_ = <SPOOL>)) { $msg .= $_ }
    close SPOOL;

    unlink "$self->{'dir'}/$key";

    return $msg;
}

=head2 $cs->clean()

Check the spool and remove files that have not been confirmed within
3 days.

=cut
sub clean {
    my ($self) = @_;
    
    # Read the content of the spool
    opendir(DIR, $self->{'dir'}) || die "can't opendir $self->{'dir'}: $!";
    my @files = grep { (! /^\./) && -f "$self->{'dir'}/$_" } readdir(DIR);
    closedir DIR;
    
    # Remove old files
    my $t = time();
    foreach (@files) {
	my @stat = stat "$self->{'dir'}/$_";
	if ($stat[9] + 86400 * 3 < $t) {
	    unlink "$self->{'dir'}/$_";
	}
    }
}

=head2 $cs->set_sendmail("/usr/sbin/sendmail");

Update the path to the sendmail binary. Sendmail is used to send
confirmation mails...

=cut
sub set_sendmail { 
    my ($self, $sendmail) = @_;
    $self->{'sendmail'} = $sendmail;
}

1;
